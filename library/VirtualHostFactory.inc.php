<?php
/**
 * Aetolos - Manager factory for virtual hosts
 *
 * Populate an array with virtual host objects
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage virtualhostfactory
 */

/**
 * Virtual host factory class
 *
 * @package aetolos
 * @subpackage virtualhostfactory
 */
class VirtualHostFactory {

	/**
	 * Populate and return an array of virtual hosts
	 * @return array
	 */
	public static function populate()
	{

		// Virtual host array
		$vhosts = array();

		// Get all virtual hosts and generate zones
		DbFactory::$db->query("SELECT Id, DomainName FROM `virtualHost`");

		// Loop
		while(DbFactory::$db->nextRow()) {

			// Load object from database
			$vhosts[DbFactory::$db->row[0]] = new VirtualHostManager();
			$vhosts[DbFactory::$db->row[0]]->domainName = DbFactory::$db->row[1];
			$vhosts[DbFactory::$db->row[0]]->get();

		}

		// Gather parked domains and check for a CA bundle
		foreach($vhosts as &$v) {

			// Skip if not a parked domain
			if($v->parkedUnder == "")
				continue;

			// Add parked domain to its parent virtual host
			$vhosts[$v->parkedUnder]->parkedDomains[] = $v;

			// Extra check for a CA bundle
			if(is_file(Config::read("pkitls|directoryCerts") . "/" . $v->domainName . ".cabundle"))
				$v->caBundle = true;

		}

		return $vhosts;

	}

	/**
	 * Check the existence of a virtual host
	 * @param string $domainName Domain name
	 * @return bool
	 */
	public static function exists($domainName)
	{

		// Get all virtual hosts and generate zones
		$preped = DbFactory::$db->conn->prepare("SELECT DomainName FROM `virtualHost` WHERE DomainName=:domainname");

		// Bind parameter
		$preped->bindParam(":domainname", $domainName);

		// Execute prepared statement
		$rc = $preped->execute();
		if($rc === false) {

			Log::error("Error while selecting virtual host from the database table: virtualHost");
			return false;

		}

		// Fetch all results in one array
		$result = $preped->fetchAll(PDO::FETCH_NUM);

		// Check the result for a possible match
		if(sizeof($result) > 0)
			return true;
		else
			return false;

	}

}

