<?php
/**
 * DaemonManager class
 *
 * A portable daemon manager for PHP projects.
 *
 * Features:
 * - Only allow PHP cli execution
 * - Set the process title based on the script filename
 * - Set user/group effective ownsership
 * - Generate a PID file
 * - Limit number of forked processes
 * - Set max execution time to zero (unlimited)
 * - Enable asynchronous signals
 * - Setup a signal handler
 * - Cleanly remove the PID file on exit
 *
 * How to use:
 * Place the DaemonManager.inc.php and DaemonInterface.inc.php
 * files in the appropriate place that will make them visible
 * to your autoloader or manually run require_once for both
 * files.
 *
 * Extend DaemonManager from your own class, create a constructor
 * if needed and call the daemon manager contructor with:
 * `parent::__construct($effectiveUser, $effectiveGroup, $filePid);`
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage daemonmanager
 */
class DaemonManager implements DaemonInterface {

	/**
	 * PID file
	 * @var string
	 */
	protected $filePid = "";

	/**
	 * Effective process user owner
	 * @var string
	 */
	protected $effectiveUser = "";

	/**
	 * Effective process group owner
	 * @var string
	 */
	protected $effectiveGroup = "";

	/**
	 * Limit the number of forked processes
	 * @var int
	 */
	private $processLimit = 1024;

	/**
	 * Processes IDs of forked children
	 * @var array<int>
	 */
	private $processList = array();

	/**
	 * Child signal reason by code
	 * @var array<int, string>
	 */
	protected $childReason = array(
		1 => "child has exited",
		2 => "child was killed",
		3 => "child terminated abnormally",
		4 => "traced child has trapped",
		5 => "child has stopped",
		6 => "stopped child has continued"
	);

	/**
	 * Constructor
	 * @param string $effectiveUser Effective process user owner
	 * @param string $effectiveGroup Effective process group owner
	 * @param string $filePid PID file
	 * @param int $processLimit Limit the number of forked processes
	 * @return void
	 */
	public function __construct(
		string $effectiveUser = "",
		string $effectiveGroup = "",
		string $filePid = "",
		int $processLimit = 1024
	) {

		// Cli execution only
		if(php_sapi_name() !== "cli") {

			Log::error("[DaemonManager: This script can only be executed under a PHP shell cli.]");
			exit(9);

		}

		// Set a custom process title instead of the default "php", which gives a better output in various tools like ps/top
		if(
			isset($_SERVER['PHP_SELF']) &&
			!empty($_SERVER['PHP_SELF'])
		)
			cli_set_process_title(basename($_SERVER['PHP_SELF']));

		// Current group
		$cGroup = posix_getgrgid(posix_getgid());
		if($cGroup === false) {

			Log::error("[DaemonManager: ERROR - Failed to get information about the current group ownership.]");
			exit(9);

		}

		// Test for group
		if(
			empty($effectiveGroup) ||
			$cGroup['name'] === $effectiveGroup
		) {

			// Store effective group
			$this->effectiveGroup = $cGroup['name'];

		} else {

			// Get group information by name
			$gInfo = posix_getgrnam($effectiveGroup);
			if($gInfo === false) {

				Log::error("[DaemonManager: ERROR - Failed to get information about the group: " . $effectiveGroup . "]");
				exit(9);

			}

			// Set effective group
			$rc = posix_setegid($gInfo['gid']);
			if($rc === false) {

				Log::error("[DaemonManager: ERROR - Failed to set the effective group to: " . $effectiveGroup . "]");
				exit(9);

			}

			// Store effective group
			$this->effectiveGroup = $effectiveGroup;

		}

		// Current user
		$cUser = posix_getpwuid(posix_getuid());
		if($cUser === false) {

			Log::error("[DaemonManager: ERROR - Failed to get information about the current user ownership.]");
			exit(9);

		}

		// Test for user
		if(
			empty($effectiveUser) ||
			$cUser['name'] === $effectiveUser
		) {

			// Store effective user
			$this->effectiveUser = $cUser['name'];

		} else {

			// Get user information by name
			$uInfo = posix_getpwnam($effectiveUser);
			if($uInfo === false) {

				Log::error("[DaemonManager: ERROR - Failed to get information about the user: " . $effectiveUser . "]");
				exit(9);

			}

			// Set effective user
			$rc = posix_seteuid($uInfo['gid']);
			if($rc === false) {

				Log::error("[DaemonManager: ERROR - Failed to set the effective user to: " . $effectiveUser . "]");
				exit(9);

			}

			// Store effective user
			$this->effectiveUser = $effectiveUser;

		}

		// Clean-up
		unset($cGroup, $gInfo, $cUser, $uInfo);

		// Store PID file
		if(!empty($filePid))
			$this->filePid = $filePid;
		else
			$this->filePid = sys_get_temp_dir() . "/daemon.pid";

		// PID file
		if(is_file($this->filePid)) {

			// PID file exists

			// Load PID
			$pid = file_get_contents($this->filePid);
			if($pid === false) {

				Log::error("[DaemonManager: ERROR - PID file error, could not read PID file]");
				exit(9);

			}

			$pid = trim($pid);
			if(!is_numeric($pid)) {

				Log::error("[DaemonManager: ERROR - PID file error, not a numeric PID]");
				exit(9);

			}

			// Scan for running PID
			$rc = posix_kill(intval($pid), 0);
			if($rc === true) {

				Log::error("[DaemonManager " . $pid . ": ERROR - Another daemon process with PID " . $pid . " is already running]");
				exit(9);

			}

		}

		// Get current PID
		$pid = getmypid();
		if($pid === false) {

			Log::error("[DaemonManager: ERROR - PID error, could not detect the current PID]");
			exit(9);

		}

		// Store current PID to file
		$rc = file_put_contents($this->filePid, strval($pid), LOCK_EX);
		if($rc === false) {

			Log::error("[DaemonManager " . $pid . ": ERROR - PID file error, could not write to PID file " . $this->filePid . "]");
			exit(9);

		}

		// Store the process limit
		$this->processLimit = $processLimit;

		// Set max execution time to zero
		set_time_limit(0);

		// PCNTL asynchronous signals
		pcntl_async_signals(true);

		// Signals
		pcntl_signal(SIGINT, array(&$this, "signalHandler"));
		pcntl_signal(SIGTERM, array(&$this, "signalHandler"));
		pcntl_signal(SIGCHLD, array(&$this, "signalHandler"));

		Log::debug("[DaemonManager " . $pid . ": Running as PID: " . $pid . "]");

		// Clean-up
		unset($pid);

	}

	/**
	 * Check the process limit
	 * @return bool
	 */
	public function checkProcessLimit(): bool
	{

		// Check the process limit
		if(sizeof($this->processList) >= $this->processLimit) {

			Log::error("[DaemonManager: process limit reached]");

			return false;

		} else {

			return true;

		}

	}

	/**
	 * Fork process and store the new PID within the process list array
	 * @return int
	 */
	public function fork(): int
	{

		// Check the process limit
		if($this->checkProcessLimit() === false)
			return -1;

		// Fork
		$newPid = pcntl_fork();

		// Parent process only
		if($newPid === -1) {

			Log::error("[DaemonManager: ERROR - failed to fork]");

		} elseif($newPid > 0) {

			// Add new forked PID to the process list
			$this->processList[] = $newPid;

		}

		return $newPid;

	}

	/**
	 * Signal handler
	 * @param int $signal Signal code
	 * @param mixed $siginfo Signal information
	 * @return void
	 */
	public function signalHandler(int $signal, $siginfo): void
	{

		switch($signal) {
			// 17
			case SIGCHLD:
				// Ignore signals from spawned processes
				if(
					!is_array($siginfo) ||
					!isset($siginfo['pid'], $siginfo['code']) ||
					!in_array($siginfo['pid'], $this->processList)
				)
					return;

				// Report code message
				if(isset($this->childReason[intval($siginfo['code'])]))
					Log::debug("[DaemonManager: " . $this->childReason[intval($siginfo['code'])] . "]");
				else
					Log::debug("[DaemonManager: unknown signal code: " . intval($siginfo['code']) . "]");

				// Child process ended, remove from the process list
				if(
					$siginfo['code'] !== CLD_STOPPED &&
					$siginfo['code'] !== CLD_CONTINUED
				)
					$this->processList = array_diff($this->processList, array($siginfo['pid']));
				return;

			// 15
			case SIGTERM:
			// 2
			case SIGINT:
				Log::debug("[DaemonManager: received signal " . $signal . "]");

				// Remove PID file
				if(is_file($this->filePid))
					unlink($this->filePid);
				exit();
		}

		Log::debug("[DaemonManager: received signal " . $signal . "]");

	}

}

