<?php
/**
 * Aetolos - Apache helper functions
 *
 * Generate a dynamic configuration based on system resources
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage apachehelper
 */

/**
 * Apache helper class
 *
 * @package aetolos
 * @subpackage apachehelper
 */
class ApacheHelper {

	/**
	 * Dynamic Apache limits
	 * @var array
	 */
	public static $apacheLimits = array();

	/**
	 * Generate dynamic config parameters based on total system memory.
	 *
	 * Axiom 1: Aetolos configures Apache to run one process per CPU core.
	 * Axiom 2: Aetolos expects the average Apache+PHP process size to be 20MB.
	 * Axiom 3: Memory is allocated first to MariaDB
	 *
	 * Based on the above axioms, the average number of concurrent connections is used to find the
	 * threads per process, which in turm gives us the following calculation:
	 *
	 * The value of maximum processes (ServerLimit) and multiplied with the value of threads per
	 * process (ThreadsPerChild) gives us the maximum number of concurrent requests that Apache
	 * can handle.
	 *
	 * ServerLimit * ThreadsPerChild = MaxRequestWorkers
	 *
	 * @return array
	 */
	public static function generateDynamicConfig()
	{

		// If already run, return results
		if(sizeof(self::$apacheLimits) > 0)
			return self::$apacheLimits;

		// Default limits (Apache v2.4 default values)
		self::$apacheLimits = array(
			'StartServers'			=> 3,
			'ServerLimit'			=> 16,
			'ThreadsPerChild'		=> 25,
			'MinSpareThreads'		=> 75,
			'ThreadLimit'			=> 64,
			'MaxRequestWorkers'		=> 400,
			'AsyncRequestWorkerFactor'	=> 2
		);

		// If MariaDB is enabled
		if(Config::read("mariadb") === "enabled") {

			// Generate a dynamic config
			MariadbHelper::generateDynamicConfig();

			// Memory usage, subtract MariaDB usage and an extra 10% for system use
			$memoryUsage = 100 - intval(Config::read("mariadb|memoryUsage")) - 10;

			// Fail-safe check
			if($memoryUsage < 10)
				// Limit memory usage to at least 10%
				$memoryUsage = 10;
			elseif($memoryUsage > 90)
				// Limit memory usage to 90% or less
				$memoryUsage = 90;

		} else {

			// Default to 90%
			$memoryUsage = 90;

		}

		// Allocate system memory to Apache + PHP-FPM
		$maxMemoryUsage = intval(Config::read("memtotal") * ($memoryUsage / 100) / 1024 / 1024);

		Log::info("Allocate " . $memoryUsage . "% of system memory to Apache/PHP-FPM: " . $maxMemoryUsage . "MB");

		// Calculate the average number of connections based on 20MB memory usage (Apache + PHP process)
		$avgConnections = intval($maxMemoryUsage / 20);

		// Based on the above number of connections, calculate the other limits
		self::$apacheLimits['StartServers'] = 1;											// Minimum processes, start with one process
		self::$apacheLimits['ServerLimit'] = intval(Config::read("cpucores"));								// Maximum processes, based CPU core count
		self::$apacheLimits['ThreadsPerChild'] = intval(floor(self::$apacheLimits['ServerLimit'] * $avgConnections));			// Threads per process, based on maximum processes
		self::$apacheLimits['MinSpareThreads'] = self::$apacheLimits['ThreadsPerChild'];						// Minimum spare threads equals the threads per process
		self::$apacheLimits['ThreadLimit'] = intval(ceil(self::$apacheLimits['ThreadsPerChild'] * 1.2));				// Maximum threads per process
		self::$apacheLimits['MaxRequestWorkers'] = intval(self::$apacheLimits['ThreadsPerChild'] * self::$apacheLimits['ServerLimit']);	// Total threads from all processes

		Log::info("Estimated average number of connections per CPU core: " . $avgConnections);
		Log::info("Apache StartServers: " . self::$apacheLimits['StartServers']);
		Log::info("Apache ServerLimit: " . self::$apacheLimits['ServerLimit']);
		Log::info("Apache ThreadsPerChild: " . self::$apacheLimits['ThreadsPerChild']);
		Log::info("Apache MinSpareThreads: " . self::$apacheLimits['MinSpareThreads']);
		Log::info("Apache ThreadLimit: " . self::$apacheLimits['ThreadLimit']);
		Log::info("Apache MaxRequestWorkers: " . self::$apacheLimits['MaxRequestWorkers']);

		// Maximum number of concurrent connections = (ThreadsPerChild + (AsyncRequestWorkerFactor * idle_workers)) * ServerLimit
		// @see https://httpd.apache.org/docs/2.4/mod/event.html
		Log::info("Apache maximum number of concurrent connections: " .
			((self::$apacheLimits['ThreadsPerChild'] + (self::$apacheLimits['AsyncRequestWorkerFactor'] * self::$apacheLimits['ThreadsPerChild'])) * self::$apacheLimits['ServerLimit']));

		// If MariaDB is enabled, check the relation between max_connections (MariaDB) and MaxRequestWorkers (Apache)
		if(
			Config::read("mariadb") === "enabled" &&
			MariadbHelper::$serverBuffers['max_connections'] < (self::$apacheLimits['MaxRequestWorkers'] / 10)
		)
			Log::warning("The maximum number of MariaDB connections (max_connections " . MariadbHelper::$serverBuffers['max_connections'] .
				") is significantly less than the maximum number of Apache requests (MaxRequestWorkers " . self::$apacheLimits['MaxRequestWorkers'] .
				"). This can lead to problems during high traffic situations, incoming connections will be processed by Apache but will" .
				" fail to connect to the database. Apache will usually report errors like: 429 Too Many Requests or 500 Internal Server Error.");

		return self::$apacheLimits;

	}

}

