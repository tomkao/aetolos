<?php
/**
 * Aetolos - Log implementation
 *
 * Support and implementation for logging events.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage log
 */

/**
 * Log implementation class
 *
 * @package aetolos
 * @subpackage log
 */
class Log implements LoggerInterface {

	/**
	 * Destination
	 * @var LogDestinationInterface
	 */
	private static $destination;

	/**
	 * Minimum priority level to log, lower levels are ignored
	 * @var int
	 */
	private static $minLevel = LOG_ERR;

	/**
	 * Default parameters
	 * @var array<string>
	 */
	private static $defaultParameters = array();

	/**
	 * Set destination.
	 * @param LogDestinationInterface $destination Log destination object
	 * @param int $minLevel Minimum priority level to log, lower levels are ignored
	 * @param array<string> $defaultParameters Default parameters to pass to the destination writer, in the form of an array of strings
	 * @return void
	 */
	public static function setDestination($destination, $minLevel = null, $defaultParameters = array())
	{

		// Set destination object
		self::$destination = $destination;

		// Set minimum priority level
		if($minLevel !== null)
			self::$minLevel = $minLevel;

		// Set default parameters
		if(sizeof($defaultParameters) > 0)
			self::$defaultParameters = $defaultParameters;

	}

	/**
	 * Emergency
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function emergency($message, $parameters = array())
	{

		if(self::$minLevel >= LOG_EMERG)
			self::$destination->write($message, LOG_EMERG, array_merge(self::$defaultParameters, $parameters));

	}

	/**
	 * Alert
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function alert($message, $parameters = array())
	{

		if(self::$minLevel >= LOG_ALERT)
			self::$destination->write($message, LOG_ALERT, array_merge(self::$defaultParameters, $parameters));

	}

	/**
	 * Critical
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function critical($message, $parameters = array())
	{

		if(self::$minLevel >= LOG_CRIT)
			self::$destination->write($message, LOG_CRIT, array_merge(self::$defaultParameters, $parameters));

	}

	/**
	 * Error
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function error($message, $parameters = array())
	{

		if(self::$minLevel >= LOG_ERR)
			self::$destination->write($message, LOG_ERR, array_merge(self::$defaultParameters, $parameters));

	}

	/**
	 * Warning
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function warning($message, $parameters = array())
	{

		if(self::$minLevel >= LOG_WARNING)
			self::$destination->write($message, LOG_WARNING, array_merge(self::$defaultParameters, $parameters));

	}

	/**
	 * Notice
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function notice($message, $parameters = array())
	{

		if(self::$minLevel >= LOG_NOTICE)
			self::$destination->write($message, LOG_NOTICE, array_merge(self::$defaultParameters, $parameters));

	}

	/**
	 * Info
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function info($message, $parameters = array())
	{

		if(self::$minLevel >= LOG_INFO)
			self::$destination->write($message, LOG_INFO, array_merge(self::$defaultParameters, $parameters));

	}

	/**
	 * Debug
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function debug($message, $parameters = array())
	{

		if(self::$minLevel >= LOG_DEBUG)
			self::$destination->write($message, LOG_DEBUG, array_merge(self::$defaultParameters, $parameters));

	}

}

