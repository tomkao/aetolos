<?php
/**
 * SocketManager class
 *
 * A socket manager based on AF_UNIX named sockets.
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage socketmanager
 */
class SocketManager extends DaemonManager implements SocketInterface {

	/**
	 * Socket buffer, in bytes (default to 8KB)
	 * @var int<0, max>
	 */
	private $buffer = 8192;

	/**
	 * Max message size, in bytes (default to 50MB)
	 * @var int<0, max>
	 */
	private $maxMessageSize = 51200000;

	/**
	 * Socket
	 * @var resource|false
	 */
	protected $socket = false;

	/**
	 * Protocol family
	 * @var int
	 */
	protected $protocolFamily = AF_UNIX;

	/**
	 * Socket file (only for AF_UNIX)
	 * @var string
	 */
	protected $fileSocket = "";

	/**
	 * IP address (for AF_INET and AF_INET6)
	 * @var string
	 */
	protected $address = "";

	/**
	 * Port (for AF_INET and AF_INET6)
	 * @var int
	 */
	protected $port = 0;

	/**
	 * Stream options
	 * @var array<string, array<string, bool|int|string>>
	 */
	protected $streamOptions = array();

	/**
	 * Stream context
	 * @var ?resource
	 */
	protected $streamContext = null;

	/**
	 * Remote IP address
	 * @var string
	 */
	protected $remoteAddress = "";

	/**
	 * HTTP reason phrase by status code
	 * @var array<int, string>
	 */
	protected $reasonPhrase = array(
		400 => "Bad Request",
		401 => "Unauthorized",
		402 => "Payment Required",
		403 => "Forbidden",
		404 => "Not Found",
		405 => "Method Not Allowed",
		406 => "Not Acceptable",
		407 => "Proxy Authentication Required",
		408 => "Request Timeout",
		409 => "Conflict",
		410 => "Gone",
		411 => "Length Required",
		412 => "Precondition Failed",
		413 => "Content Too Large",
		414 => "URI Too Long",
		415 => "Unsupported Media Type",
		416 => "Range Not Satisfiable",
		417 => "Expectation Failed",
		418 => "I'm a teapot",
		421 => "Misdirected Request",
		422 => "Unprocessable Content",
		423 => "Locked",
		424 => "Failed Dependency",
		425 => "Too Early",
		426 => "Upgrade Required",
		428 => "Precondition Required",
		429 => "Too Many Requests",
		431 => "Request Header Fields Too Large",
		451 => "Unavailable For Legal Reasons",
		500 => "Internal Server Error",
		501 => "Not Implemented",
		502 => "Bad Gateway",
		503 => "Service Unavailable",
		504 => "Gateway Timeout",
		505 => "HTTP Version Not Supported",
		506 => "Variant Also Negotiates",
		507 => "Insufficient Storage",
		508 => "Loop Detected",
		510 => "Not Extended",
		511 => "Network Authentication Required",
	);

	/**
	 * Constructor
	 * @param string $effectiveUser Effective process user owner
	 * @param string $effectiveGroup Effective process group owner
	 * @param string $filePid PID file
	 * @param int $processLimit Limit the number of forked processes
	 * @param string $connection Socket file or IPv4/IPv6 address/port string ("unix|socket:/path/to/file.sock" or "inet:port@IP")
	 * @param array<string, bool|int|string> $ssl SSL/TLS context options (@see https://www.php.net/manual/en/context.ssl.php)
	 * @return void
	 */
	public function __construct(
		string $effectiveUser = "",
		string $effectiveGroup = "",
		string $filePid = "",
		int $processLimit = 1024,
		string $connection = "",
		array $ssl = array()
	) {

		// Input validation
		if(
			empty($connection) ||
			strpos($connection, ":") === false
		) {

			Log::error("[SocketManager: invalid connection string]");
			exit(9);

		}

		// Separate at the ":" character
		$connectionArray = explode(":", $connection);
		if(
			sizeof($connectionArray) !== 2 ||
			!isset($connectionArray[0]) ||
			!isset($connectionArray[1])
		) {

			Log::error("[SocketManager: invalid connection string]");
			exit(9);

		}

		// Switch between connection types
		switch($connectionArray[0]) {
			case "unix":
			case "socket":
				// Store socket file
				$this->fileSocket = $connectionArray[1];

				// Protocol family
				$this->protocolFamily = AF_UNIX;
				break;

			case "inet":
				// Input validation
				if(strpos($connectionArray[1], "@") === false) {

					Log::error("[SocketManager: invalid connection string, missing port@address separator]");
					exit(9);

				}

				// Separate at the "@" character
				$connectionArray = explode("@", $connectionArray[1]);
				if(
					sizeof($connectionArray) !== 2 ||
					!isset($connectionArray[0]) ||
					!is_numeric($connectionArray[0]) ||
					!isset($connectionArray[1])
				) {

					Log::error("[SocketManager: invalid connection string]");
					exit(9);

				}

				// Store port
				$this->port = intval($connectionArray[0]);

				// Store address
				$this->address = $connectionArray[1];

				// Protocol family
				if(strpos($this->address, ":") !== false)
					$this->protocolFamily = AF_INET6;
				else
					$this->protocolFamily = AF_INET;
				break;

			default:
				Log::error("[SocketManager: invalid connection string, unknown connection type]");
				exit(9);
		}

		// Merge SSL/TLS context options
		if(sizeof($ssl) > 0) {

			// Test for the certificate authority file
			if(
				isset($ssl['cafile']) &&
				(
					!is_string($ssl['cafile']) ||
					!is_file($ssl['cafile']) ||
					!is_readable($ssl['cafile'])
				)
			) {

				Log::error("[SocketManager: invalid cafile, file not found or not readable]");
				exit(9);

			}

			// Test for the certificate directory
			if(
				isset($ssl['capath']) &&
				(
					!is_string($ssl['capath']) ||
					!is_dir($ssl['capath'])
				)
			) {

				Log::error("[SocketManager: invalid capath, directory not found]");
				exit(9);

			}

			// Test for the public certificate file
			if(
				isset($ssl['local_cert']) &&
				(
					!is_string($ssl['local_cert']) ||
					!is_file($ssl['local_cert']) ||
					!is_readable($ssl['local_cert'])
				)
			) {

				Log::error("[SocketManager: invalid local_cert, file not found or not readable]");
				exit(9);

			}

			// Test for the private certificate key file
			if(
				isset($ssl['local_pk']) &&
				(
					!is_string($ssl['local_pk']) ||
					!is_file($ssl['local_pk']) ||
					!is_readable($ssl['local_pk'])
				)
			) {

				Log::error("[SocketManager: invalid local_pk, file not found or not readable]");
				exit(9);

			}

			$this->streamOptions['ssl'] = $ssl;

		}

		// Call the daemon manager constructor
		parent::__construct($effectiveUser, $effectiveGroup, $filePid, $processLimit);

	}

	/**
	 * Prepare UNIX socket (Create, Bind, NonBlock, Listen)
	 * @return bool
	 */
	private function prepareSocket(): bool
	{

		Log::debug("[SocketManager: socket create]");

		// Create socket
		$this->socket = @socket_create($this->protocolFamily, SOCK_STREAM, 0);
		if($this->socket === false) {

			Log::debug("[SocketManager: ERROR - failed to create socket: " . socket_strerror(socket_last_error()) . "]");
			return false;

		}

		// Check for an old socket file and remove it
		if(file_exists($this->fileSocket)) {

			Log::debug("[SocketManager: remove old socket: " . $this->fileSocket . "]");

			@unlink($this->fileSocket);

		}

		Log::debug("[SocketManager: socket bind]");

		// Bind socket
		$rc = @socket_bind($this->socket, $this->fileSocket);
		if($rc === false) {

			// Check for an old socket file and remove it
			if(file_exists($this->fileSocket))
				@unlink($this->fileSocket);

			Log::debug("[SocketManager: ERROR - failed to bind socket: " . socket_strerror(socket_last_error($this->socket)) . "]");
			return false;

		}

		Log::debug("[SocketManager: set socket permissions]");

		// Set limited socket permissions
		$rc = chmod($this->fileSocket, 0660);
		if($rc === false) {

			Log::debug("[SocketManager: ERROR - failed to set limited permissions to the socket: " . $this->fileSocket . "]");
			return false;

		}

		Log::debug("[SocketManager: set nonblocking mode]");

		// Set non blocking mode
		socket_set_nonblock($this->socket);

		Log::debug("[SocketManager: socket listen]");

		// Listen socket (no blacklog connections)
		$rc = @socket_listen($this->socket, 0);
		if($rc === false)
			Log::debug("[SocketManager: ERROR - failed to listen to socket: " . socket_strerror(socket_last_error($this->socket)) . "]");

		return $rc;

	}

	/**
	 * Prepare INET address and port (Create, Bind, NonBlock, Listen)
	 * @return bool
	 */
	private function prepareInet(): bool
	{

		// Input validation
		if(
			empty($this->address) ||
			$this->port === 0
		)
			return false;

		Log::debug("[SocketManager: inet create]");

		// Create socket
		$this->socket = @socket_create($this->protocolFamily, SOCK_STREAM, SOL_TCP);
		if($this->socket === false) {

			Log::debug("[SocketManager: ERROR - failed to create socket: " . socket_strerror(socket_last_error()) . "]");
			return false;

		}

		Log::debug("[SocketManager: socket bind]");

		// Bind socket
		$rc = @socket_bind($this->socket, $this->address, $this->port);
		if($rc === false) {

			Log::debug("[SocketManager: ERROR - failed to bind socket: " . socket_strerror(socket_last_error($this->socket)) . "]");
			return false;

		}

		Log::debug("[SocketManager: set nonblocking mode]");

		// Set non blocking mode
		socket_set_nonblock($this->socket);

		Log::debug("[SocketManager: disable linger mode]");

		// Disable linger mode (drop buffer data on socket close)
		socket_set_option($this->socket, SOL_SOCKET, SO_LINGER, array('l_onoff' => 1, 'l_linger' => 0));

		Log::debug("[SocketManager: socket listen]");

		// Listen socket (no blacklog connections)
		$rc = @socket_listen($this->socket, 0);
		if($rc === false)
			Log::debug("[SocketManager: ERROR - failed to listen to socket: " . socket_strerror(socket_last_error($this->socket)) . "]");

		return $rc;

	}

	/**
	 * Prepare stream socket (Create, Server, NonBlock)
	 * @return bool
	 */
	private function prepareStream(): bool
	{

		Log::debug("[SocketManager: stream get transports]");

		// Ensure the SSL/TLS transport exists (which covers all TLS versions)
		if(!in_array("ssl", stream_get_transports(), true)) {

			Log::error("[SocketManager: the ssl transport is not available]");
			return false;

		}

		Log::debug("[SocketManager: stream context create]");

		// Create the stream context
		$this->streamContext = stream_context_create($this->streamOptions);

		// Prepare error variables
		$errorCode = 0;
		$errorMessage = "";

		Log::debug("[SocketManager: stream socket server]");

		// Create the stream socket
		$this->socket = stream_socket_server(
			"tcp://" . $this->address . ":" . $this->port,
			$errorCode,
			$errorMessage,
			STREAM_SERVER_BIND | STREAM_SERVER_LISTEN,
			$this->streamContext
		);
		if($this->socket === false) {

			Log::error("[SocketManager: stream_socket_server failed with: " . $errorMessage . " (" . $errorCode . ")]");
			return false;

		}

		Log::debug("[SocketManager: set nonblocking mode]");

		// Set non blocking mode
		stream_set_blocking($this->socket, false);

		return true;

	}

	/**
	 * Loop socket
	 * @return bool
	 */
	private function loopSocket(): bool
	{

		// Input validation
		if($this->socket === false) {

			Log::error("[SocketManager: socket not connected]");
			return false;

		}

		// Loop
		while(true) {

			// Read copy of socket(s)
			$read = array($this->socket);
			$write = null;
			$except	= null;

			Log::debug("[SocketManager: socket select...]");

			// Socket select
			$rc = @socket_select($read, $write, $except, null);
			if($rc === false) {

				usleep(100);
				continue;

			}

			Log::debug("[SocketManager: new socket accept]");

			// Accept connection
			$newSocket = @socket_accept($this->socket);
			if($newSocket === false) {

				usleep(100);
				continue;

			} elseif($newSocket < 1) {

				usleep(100);
				continue;

			}

			// Exclude socket connections
			if($this->protocolFamily !== AF_UNIX) {

				// Get remote client IP address
				$rc = socket_getpeername($newSocket, $this->remoteAddress);
				if($rc === false)
					$this->remoteAddress = "";

			}

			if(!empty($this->remoteAddress))
				Log::debug("[SocketManager: new connection accepted from " . $this->remoteAddress . ", fork...]");
			else
				Log::debug("[SocketManager: new connection accepted, fork...]");

			// DaemonManager fork
			$newPid = $this->fork();
			if($newPid === 0) {

				// Child process

				Log::debug("[SocketManager: I am a child]");

				// Store new socket
				$this->socket = $newSocket;

				Log::debug("[SocketManager: set blocking mode]");

				// Set blocking mode
				socket_set_block($this->socket);

				Log::debug("[SocketManager: set a 10 second timeout]");

				socket_set_option($this->socket, SOL_SOCKET, SO_SNDTIMEO, array('sec' => 10, 'usec' => 0));
				socket_set_option($this->socket, SOL_SOCKET, SO_RCVTIMEO, array('sec' => 10, 'usec' => 0));

				// Leave the file socket to be handled by the parent
				$this->fileSocket = "";

				// Classes that extend the SocketManager must have a processChild method
				if(method_exists($this, "processChild"))
					$this->processChild();

				// Child process exit here
				exit();

			}

			// Parent process continue loop

		}

	}

	/**
	 * Loop stream
	 * @return bool
	 */
	private function loopStream(): bool
	{

		// Input validation
		if($this->socket === false) {

			Log::error("[SocketManager: socket not connected]");
			return false;

		}

		// Loop
		while(true) {

			// Check the process limit
			if($this->checkProcessLimit() === false) {

				// Too many forked processes

				// Wait for 1 second
				sleep(1);

				// Try again
				continue;

			}

			// Read copy of socket(s)
			$read = array($this->socket);
			$write = null;
			$except	= null;

			Log::debug("[SocketManager: stream select...]");

			// Stream select
			$rc = @stream_select($read, $write, $except, null);
			if($rc === false) {

				usleep(100);
				continue;

			}

			Log::debug("[SocketManager: stream socket accept]");

			// New socket accept
			$newSocket = @stream_socket_accept($this->socket, 10, $this->remoteAddress);
			if($newSocket === false) {

				usleep(100);
				continue;

			}

			// Parse the remote address and remove the port
			$rc = explode(":", $this->remoteAddress);
			if(isset($rc[0]))
				$this->remoteAddress = $rc[0];
			else
				$this->remoteAddress = "";

			if(!empty($this->remoteAddress))
				Log::debug("[SocketManager: new connection accepted from " . $this->remoteAddress . ", fork...]");
			else
				Log::debug("[SocketManager: new connection accepted, fork...]");

			// DaemonManager fork
			$newPid = $this->fork();
			if($newPid === 0) {

				// Child process

				Log::debug("[SocketManager: I am a child]");

				// Store new socket
				$this->socket = $newSocket;

				Log::debug("[SocketManager: set blocking mode]");

				// Set blocking mode
				stream_set_blocking($this->socket, true);

				Log::debug("[SocketManager: set a 10 second timeout]");

				stream_set_timeout($this->socket, 10, 0);

				// Enable SSL/TLS on TCP socket
				$rc = stream_socket_enable_crypto($this->socket, true, STREAM_CRYPTO_METHOD_ANY_SERVER);
				if($rc === false) {

					Log::debug("[SocketManager: failed to enable SSL/TLS crypto]");

					exit();

				} elseif($rc === 0) {

					Log::debug("[SocketManager: socket not set to blocking mode]");

					exit();

				}

				// Leave the file socket to be handled by the parent
				$this->fileSocket = "";

				// Classes that extend the SocketManager must have a processChild method
				if(method_exists($this, "processChild"))
					$this->processChild();

				// Child process exit here
				exit();

			}

			// Parent process continue loop

		}

	}

	/**
	 * Accept connections
	 * @return bool
	 */
	public function acceptConnections(): bool
	{

		// Switch between protocol families
		switch($this->protocolFamily) {
			case AF_UNIX:
				Log::debug("[SocketManager: prepare (UNIX socket)]");

				// Prepare socket
				$rc = $this->prepareSocket();
				if($rc === false) {

					Log::error("[SocketManager: ERROR - failed to prepare]");
					return false;

				}

				// Loop
				return $this->loopSocket();
				// no break

			case AF_INET:
			case AF_INET6:
				Log::debug("[SocketManager: prepare (IP address)]");

				// Prepare IP address (encrypted streams or plain sockets)
				if(isset($this->streamOptions['ssl']))
					$rc = $this->prepareStream();
				else
					$rc = $this->prepareInet();
				if($rc === false) {

					Log::error("[SocketManager: ERROR - failed to prepare]");
					return false;

				}

				// Loop
				if(isset($this->streamOptions['ssl']))
					return $this->loopStream();
				else
					return $this->loopSocket();
				// no break

			default:
				Log::error("[SocketManager: ERROR - failed to prepare]");
				return false;
		}

	}

	/**
	 * Read HTTP formatted string from stream socket
	 * @return array{method: string, request-uri: string, http-version: string, header: array<string>, body: string, status-code: int}
	 */
	public function socketReadByHttp(): array
	{

		$message = array(
			'method'	=> "",
			'request-uri'	=> "",
			'http-version'	=> "",
			'header'	=> array(),
			'body'		=> "",
			'status-code'	=> 200
		);

		// Input validation
		if($this->socket === false) {

			Log::error("[SocketManager: socket not connected]");

			$message['status-code'] = 500;
			return $message;

		}

		Log::debug("[SocketManager: socket read header]");

		// Read from socket
		if(isset($this->streamOptions['ssl']))
			$input = @fread($this->socket, $this->buffer);
		else
			$input = @socket_read($this->socket, $this->buffer, PHP_BINARY_READ);

		if(
			$input === false ||
			empty($input)
		) {

			// 400 Bad Request
			$message['status-code'] = 400;
			return $message;

		}

		Log::debug("[SocketManager: client said... '" . print_r(substr(strval(preg_replace('/[[:^print:]]/', "", $input)), 0, 10), true) . "']");

		// Find the end of line
		$requestLineEnd = strpos($input, "\r\n");
		if($requestLineEnd === false) {

			// 400 Bad Request
			$message['status-code'] = 400;
			return $message;

		}

		// Extract the request-line
		$requestLine = substr($input, 0, $requestLineEnd);

		// Remove the request-line from the rest of the input buffer
		$message['body'] = substr($input, $requestLineEnd + 2);

		// Clean-up
		$input = "";

		// Limit by size
		if(empty($requestLine)) {

			// 400 Bad Request
			$message['status-code'] = 400;
			return $message;

		} elseif(strlen($requestLine) > 255) {

			// 414 URI Too Long
			$message['status-code'] = 414;
			return $message;

		}

		// Split request-line
		$rc = explode(" ", $requestLine);

		// Calculate the size
		$requestLineSize = sizeof($rc);
		if($requestLineSize === 2) {

			// HTTP/0.9

			// 426 Upgrade Required
			$message['status-code'] = 426;
			return $message;

		} elseif($requestLineSize !== 3) {

			// Not HTTP/1.x?

			// 400 Bad Request
			$message['status-code'] = 400;
			return $message;

		}

		// Validate the HTTP method format
		if(preg_match('/^[A-Z]*$/D', $rc[0]) !== 1) {

			// 405 Method Not Allowed
			$message['status-code'] = 405;
			return $message;

		}

		// Validate the HTTP version format
		if(preg_match('/^HTTP\/\d\.\d$/D', $rc[2]) !== 1) {

			// 400 Bad Request
			$message['status-code'] = 400;
			return $message;

		}

		// Place request-line parts into their corresponding array key
		$message['method'] = $rc[0];
		$message['request-uri'] = $rc[1];
		$message['http-version'] = $rc[2];

		// If we do not have all data, read for more
		if(strpos($message['body'], "\r\n\r\n") === false) {

			// Loop
			while(true) {

				Log::debug("[SocketManager: socket read]");

				// Read from socket
				if(isset($this->streamOptions['ssl']))
					$input = @fread($this->socket, $this->buffer);
				else
					$input = @socket_read($this->socket, $this->buffer, PHP_BINARY_READ);
				if(
					$input === false ||
					empty($input)
				)
					break;

				Log::debug("[SocketManager: client said... '" . print_r(substr(strval(preg_replace('/[[:^print:]]/', "", $input)), 0, 10), true) . "']");

				$message['body'] .= $input;

				// Test message size
				if(strlen($message['body']) > $this->maxMessageSize) {

					// 413 Content Too Large
					$message['status-code'] = 413;
					return $message;

				}

				// Stop reading from the socket once we reach \R|N\R\N
				if(strpos($message['body'], "\r\n\r\n") !== false)
					break;

			}

		}

		// Clean-up
		unset($input);

		// Find the separator of the header
		$headerPos = strpos($message['body'], "\r\n\r\n");
		if($headerPos === false) {

			// 400 Bad Request
			$message['status-code'] = 400;
			return $message;

		}

		// Extract the header lines
		$message['header'] = array_merge($message['header'], explode("\r\n", substr($message['body'], 0, $headerPos)));

		// Only keep the body
		$message['body'] = substr($message['body'], $headerPos + 4);

		// Extract the Content-Length header
		$rc = preg_grep('/^Content-Length: /i', $message['header']);
		if(
			(
				$rc === false ||
				sizeof($rc) === 0
			) &&
			in_array($message['method'], array("PUT", "PATCH"))
		) {

			// The Content-Length is required but not supplied

			// 400 Bad Request
			$message['status-code'] = 400;
			return $message;

		} elseif($rc !== false) {

			// Extract the length
			$contentLength = intval(preg_replace('/^Content-Length: /', "", array_shift($rc), 1));

			// Loop
			while(strlen($message['body']) < $contentLength) {

				Log::debug("[SocketManager: socket read]");

				// Read from socket
				if(isset($this->streamOptions['ssl']))
					$input = @fread($this->socket, $this->buffer);
				else
					$input = @socket_read($this->socket, $this->buffer, PHP_BINARY_READ);
				if(
					$input === false ||
					empty($input)
				)
					break;

				Log::debug("[SocketManager: client said... '" . print_r(substr(strval(preg_replace('/[[:^print:]]/', "", $input)), 0, 10), true) . "']");

				$message['body'] .= $input;

				// Test message size
				if(strlen($message['body']) > $this->maxMessageSize) {

					// 413 Content Too Large
					$message['status-code'] = 413;
					return $message;

				}

			}

		}

		Log::debug("[SocketManager: message body: '" . print_r(substr(strval(preg_replace('/[[:^print:]]/', "", $message['body'])), 0, 80), true) . "']");

		// Return the message
		return $message;

	}

	/**
	 * Write HTTP formatted string to stream socket
	 * @param array{http-version: string, status-code: int, reason-phrase: string, header: array<string>, body: string} $message Message
	 * @return bool
	 */
	public function socketWriteByHttp(array $message): bool
	{

		// Input validation
		if($this->socket === false) {

			Log::error("[SocketManager: socket not connected]");
			return false;

		}

		if(
			!isset(
				$message['http-version'],
				$message['status-code'],
				$message['reason-phrase'],
				$message['header'],
				$message['body']
			) ||
			!is_string($message['http-version']) ||
			!is_int($message['status-code']) ||
			!is_string($message['reason-phrase']) ||
			!is_array($message['header']) ||
			!is_string($message['body'])
		) {

			Log::error("[SocketManager: invalid message format]");
			return false;

		}

		// Add a host header
		if(
			$this->protocolFamily === AF_INET6 ||
			$this->protocolFamily === AF_INET
		) {

			$rc = preg_grep('/^Host: /i', $message['header']);
			if(
				$rc === false ||
				sizeof($rc) === 0
			)
				$message['header'] = array_merge(array("Host: " . $this->address . ":" . $this->port), $message['header']);

		}

		// Add a date header
		$rc = preg_grep('/^Date: /i', $message['header']);
		if(
			$rc === false ||
			sizeof($rc) === 0
		)
			$message['header'][] = "Date: " . str_replace("+0000", "GMT", gmdate("r"));

		// Add a connection header
		$rc = preg_grep('/^Connection: /i', $message['header']);
		if(
			$rc === false ||
			sizeof($rc) === 0
		)
			$message['header'][] = "Connection: close";

		// Add a content-length header
		$contentLength = strlen($message['body']);
		if($contentLength > 0) {

			$rc = preg_grep('/^Content-Length: /i', $message['header']);
			if(
				$rc === false ||
				sizeof($rc) === 0
			)
				$message['header'][] = "Content-Length: " . $contentLength;

		}

		// Get a default reason phrase, if one was not provided
		if(
			empty($message['reason-phrase']) &&
			isset($this->reasonPhrase[$message['status-code']])
		)
			$message['reason-phrase'] = $this->reasonPhrase[$message['status-code']];

		// Message array to string
		$message = $message['http-version'] . " " .
			$message['status-code'] . " " .
			$message['reason-phrase'] . "\r\n" .
			implode("\r\n", $message['header']) . "\r\n\r\n" .
			$message['body'];

		// Length of message
		$length = strlen($message);

		Log::debug("[SocketManager: message length: " . $length . "]");

		Log::debug("[SocketManager: start of socket write loop]");

		// Loop
		while(true) {

			Log::debug("[SocketManager: socket write for length: " . $length . "]");

			// Send bytes to socket
			if(isset($this->streamOptions['ssl']))
				$sent = @fwrite($this->socket, $message, $length);
			else
				$sent = @socket_write($this->socket, $message, $length);
			if(
				$sent === false ||
				$sent === 0
			) {

				Log::debug("[SocketManager: socket write ended]");
				break;

			}

			Log::debug("[SocketManager: bytes sent: " . print_r($sent, true) . "]");

			// Check sent bytes
			if($sent < $length) {

				Log::debug("[SocketManager: unsent bytes in buffer]");

				// Remaining bytes
				$message = substr($message, $sent);

				// Length of remaining bytes
				$length -= $sent;

				// Fail-safe
				if($length <= 0)
					break;

			} else {

				Log::debug("[SocketManager: buffer empty]");

				// Nothing left to send
				$length = 0;

				break;

			}

		}

		if($length === 0) {

			Log::debug("[SocketManager: message sent]");
			return true;

		} else {

			Log::debug("[SocketManager: message buffer not empty (" . $length . ")]");
			return false;

		}

	}

	/**
	 * Read string terminated by a NUL from socket
	 * @return string
	 */
	public function socketReadByNul(): string
	{

		// Input validation
		if($this->socket === false) {

			Log::error("[SocketManager: socket not connected]");
			return "";

		}

		$message = "";

		while(true) {

			Log::debug("[SocketManager: socket read]");

			// Read from socket
			$input = @socket_read($this->socket, $this->buffer, PHP_BINARY_READ);
			if($input === false)
				break;

			Log::debug("[SocketManager: client said... '" . print_r(substr(strval(preg_replace('/[[:^print:]]/', "", $input)), 0, 10), true) . "']");

			$message .= $input;

			// Test message size
			if(strlen($message) > $this->maxMessageSize) {

				Log::error("[SocketManager: message exceedes size limit (" . $this->maxMessageSize . ")]");
				return "";

			}

			// Stop reading from the socket once we reach a NUL character
			if(strpos($input, "\0") !== false)
				break;

		}

		Log::debug("[SocketManager: final message: '" . print_r(substr(strval(preg_replace('/[[:^print:]]/', "", $message)), 0, 80), true) . "']");

		// Return the message without the NUL character
		return rtrim($message, "\0");

	}

	/**
	 * Write string terminated by NUL to socket
	 * @param string $message Message
	 * @return bool
	 */
	public function socketWriteByNul(string $message): bool
	{

		// Input validation
		if($this->socket === false) {

			Log::error("[SocketManager: socket not connected]");
			return false;

		}

		// Add a NUL character at the end of the message
		$message .= "\0";

		// Length of message string
		$length = strlen($message);

		Log::debug("[SocketManager: message length: " . $length . "]");

		Log::debug("[SocketManager: start of socket write loop]");

		// Loop
		while(true) {

			Log::debug("[SocketManager: socket write for length: " . $length . "]");

			// Send bytes to socket
			$sent = @socket_write($this->socket, $message, $length);
			if($sent === false) {

				Log::debug("[SocketManager: socket write ended]");
				break;

			}

			Log::debug("[SocketManager: bytes sent: " . print_r($sent, true) . "]");

			// Check sent bytes
			if($sent < $length) {

				Log::debug("[SocketManager: unsent bytes in buffer]");

				// Remaining bytes
				$message = substr($message, $sent);

				// Length of remaining bytes
				$length -= $sent;

			} else {

				Log::debug("[SocketManager: buffer empty]");

				// Nothing left to send
				$length = 0;

				break;

			}

		}

		if($length === 0) {

			Log::debug("[SocketManager: message sent]");
			return true;

		} else {

			Log::debug("[SocketManager: message buffer not empty (" . $length . ")]");
			return false;

		}

	}

	/**
	 * Read string prefixed by the size from socket (uint32)
	 * @return string
	 */
	public function socketReadBySize(): string
	{

		// Input validation
		if($this->socket === false) {

			Log::error("[SocketManager: socket not connected]");
			return "";

		}

		$message = "";

		Log::debug("[SocketManager: socket read size prefix]");

		// Read from socket
		$sizeBinary = @socket_read($this->socket, 4, PHP_BINARY_READ);
		if(
			$sizeBinary === false ||
			empty($sizeBinary)
		)
			return $message;

		// Get the size of the message (uint32)
		$size = unpack("N", substr($sizeBinary, 0, 4));
		if(
			$size === false ||
			!isset($size[1])
		)
			return $message;
		else
			$size = intval($size[1]);

		Log::debug("[SocketManager: expected size: " . $size . "]");

		Log::debug("[SocketManager: socket read]");

		// Read from socket
		$message = @socket_read($this->socket, $size, PHP_BINARY_READ);
		if($message === false)
			return "";

		// Test message size
		if(strlen($message) > $this->maxMessageSize) {

			Log::error("[SocketManager: message exceedes size limit (" . $this->maxMessageSize . ")]");
			return "";

		}

		Log::debug("[SocketManager: final message (size " . strlen($message) . " bytes): '" . print_r(substr(strval(preg_replace('/[[:^print:]]/', "", $message)), 0, 80), true) . "']");

		// Return the message with the size prefix
		return $sizeBinary . $message;

	}

	/**
	 * Write string prefixed by the size to socket (uint32)
	 * @param string $message Message
	 * @param bool $includesSizePrefix True implies that the message already includes a size prefix
	 * @return bool
	 */
	public function socketWriteBySize(string $message, bool $includesSizePrefix = false): bool
	{

		// Input validation
		if($this->socket === false) {

			Log::error("[SocketManager: socket not connected]");
			return false;

		}

		// Length of message string
		$length = strlen($message);

		// Generate a size prefix if one is not already included in the message
		if($includesSizePrefix === false) {

			// Prefix the length of the message in uint32 format
			$message = pack("N", $length) . $message;

			// Recalculate the prefixed message
			$length = strlen($message);

		}

		Log::debug("[SocketManager: message length: " . $length . "]");

		Log::debug("[SocketManager: start of socket write loop]");

		// Loop
		while(true) {

			Log::debug("[SocketManager: socket write for length: " . $length . "]");

			// Send bytes to socket
			$sent = @socket_write($this->socket, $message, $length);
			if($sent === false) {

				Log::debug("[SocketManager: socket write ended]");
				break;

			}

			Log::debug("[SocketManager: bytes sent: " . print_r($sent, true) . "]");

			// Check sent bytes
			if($sent < $length) {

				Log::debug("[SocketManager: unsent bytes in buffer]");

				// Remaining bytes
				$message = substr($message, $sent);

				// Length of remaining bytes
				$length -= $sent;

			} else {

				Log::debug("[SocketManager: buffer empty]");

				// Nothing left to send
				$length = 0;

				break;

			}

		}

		if($length === 0) {

			Log::debug("[SocketManager: message sent]");
			return true;

		} else {

			Log::debug("[SocketManager: message buffer not empty (" . $length . ")]");
			return false;

		}

	}

	/**
	 * Initiate a remote connection
	 * @param ?string $address Address (AF_INET & AF_INET6) or file socket (AF_UNIX)
	 * @param ?int $port Port (AF_INET & AF_INET6)
	 * @return boolean
	 */
	public function remoteConnect(?string $address = null, ?int $port = null): bool
	{

		// Use the passed values or use the ones stored by the constructor
		if($address === null)
			$address = $this->address;
		if($port === null)
			$port = $this->port;

		Log::debug("[SocketManager: create socket]");

		// Create socket
		$this->socket = @socket_create($this->protocolFamily, SOCK_STREAM, 0);
		if($this->socket === false) {

			Log::debug("[SocketManager: ERROR - failed to create socket: " . socket_strerror(socket_last_error()) . "]");
			return false;

		}

		// Set non blocking mode
		socket_set_nonblock($this->socket);

		Log::debug("[SocketManager: connect to socket]");

		do {

			// Connect to socket
			$rc = @socket_connect($this->socket, $address, $port);
			if($rc === false) {

				// Get error number
				$error = socket_last_error($this->socket);

				// Ignore non-errors related to connection status
				if($error !== SOCKET_EALREADY && $error !== SOCKET_EINPROGRESS) {

					Log::debug("[SocketManager: ERROR - failed to connect to socket " . $address . ": " . socket_strerror($error) . "]");

					// Set blocking mode
					socket_set_block($this->socket);

					return false;

				}

			}

			usleep(100);

		} while($rc !== false);

		// Set blocking mode
		socket_set_block($this->socket);

		return true;

	}

	/**
	 * Destructor
	 * @return void
	 */
	public function __destruct()
	{

		// Close socket
		if($this->socket !== false) {

			Log::debug("[SocketManager: shutdown and close socket]");

			// Switch between protocol families
			switch($this->protocolFamily) {
				case AF_UNIX:
					// Close UNIX socket
					@socket_shutdown($this->socket, 2);
					@socket_close($this->socket);
					break;

				case AF_INET:
				case AF_INET6:
					if(isset($this->streamOptions['ssl'])) {

						// Close SSL/TLS encrypted socket
						@stream_socket_shutdown($this->socket, 2);
						@fclose($this->socket);

					} else {

						// Close plain TCP socket
						@socket_shutdown($this->socket, 2);
						@socket_close($this->socket);

					}
					break;
			}

			// Remove socket file
			if(
				!empty($this->fileSocket) &&
				file_exists($this->fileSocket)
			)
				unlink($this->fileSocket);

		}

	}

}

