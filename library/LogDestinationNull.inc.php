<?php
/**
 * Aetolos - Log destination to a black hole
 *
 * Do not log events
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage logdestinationnull
 */

/**
 * Do not log events implementation class
 *
 * @package aetolos
 * @subpackage logdestinationnull
 */
class LogDestinationNull implements LogDestinationInterface {

	/**
	 * Constructor.
	 * @param string $output
	 * @return void
	 */
	public function __construct($output = "")
	{

		// No-op

	}

	/**
	 * Null write
	 * @param string $string Message string
	 * @param int $priorityLevel Priority level
	 * @param array<string> $parameters Extra parameters to pass to the destination writer, in the form of an array of strings
	 * @return void
	 */
	public function write($string, $priorityLevel, $parameters = array())
	{

		// No-op

	}

}

