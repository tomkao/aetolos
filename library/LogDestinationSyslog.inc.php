<?php
/**
 * Aetolos - Log destination to syslog implementation
 *
 * Submit log events to syslog
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage logdestinationsyslog
 */

/**
 * Log destination to syslog implementation class
 *
 * @package aetolos
 * @subpackage logdestinationsyslog
 */
class LogDestinationSyslog implements LogDestinationInterface {

	/**
	 * Syslog prefix
	 * @var string
	 */
	private $prefix = "";

	/**
	 * Constructor.
	 * @param string $output
	 * @return void
	 */
	public function __construct($output = "")
	{

		// Set the prefix based on the script filename
		if(
			isset($_SERVER['PHP_SELF']) &&
			!empty($_SERVER['PHP_SELF'])
		)
			$this->prefix = basename($_SERVER['PHP_SELF']);
		else
			$this->prefix = "daemon";

	}

	/**
	 * Write string to syslog
	 * @param string $string Message string
	 * @param int $priorityLevel Priority level
	 * @param array<string> $parameters Extra parameters to pass to the destination writer, in the form of an array of strings
	 * @return void
	 */
	public function write($string, $priorityLevel, $parameters = array())
	{

		// Open syslog with a prefix, supply the PID along with the message, use the DAEMON facility
		openlog($this->prefix, LOG_PID, LOG_USER);

		// Generate a system log message
		syslog($priorityLevel, $string);

	}

}

