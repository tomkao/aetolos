<?php
/**
 * Aetolos - Helper functions for working with configuration files
 *
 * Manipulate configuration (.ini or .conf) parameters, without unnecessary modifications.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage confighelper
 */

/**
 * ConfigHelper trait
 *
 * @package aetolos
 * @subpackage confighelper
 */
trait ConfigHelper {

	/**
	 * Override template file
	 * @param string $templateFile Template file which contains Smarty blocks
	 * @param Smarty|null &$smarty Smarty object (optional)
	 * @return array
	 */
	protected function selectTemplateFile($templateFile, &$smarty = null)
	{

		// Start with no override files
		$prependFile = "";
		$overrideFile = "";
		$appendFile = "";

		// First try: search under virtual host subdirectories
		if($smarty !== null) {

			// Get current virtual host
			$vHost = $smarty->getTemplateVars("SERVERNAME");
			if(
				$vHost !== null &&
				$vHost !== false
			) {

				// Override template file
				$rc = dirname($templateFile) . "/templates.d/" . $vHost . "/" . basename($templateFile);

				// Test for the existence of an override file
				if(is_file($rc)) {

					Log::debug("Override template file: " . $rc);

					// Set override template file
					$overrideFile = $rc;

				}

				// Override prepend file
				$rc = dirname($templateFile) . "/templates.d/" . $vHost .
					"/" . str_replace(".tpl", "_prepend.tpl", basename($templateFile));

				// Test for the existence of a prepend file
				if(is_file($rc)) {

					Log::debug("Override prepend file: " . $rc);

					// Set override prepend file
					$prependFile = $rc;

				}

				// Override append file
				$rc = dirname($templateFile) . "/templates.d/" . $vHost .
					"/" . str_replace(".tpl", "_append.tpl", basename($templateFile));

				// Test for the existence of an append file
				if(is_file($rc)) {

					Log::debug("Override append file: " . $rc);

					// Set override append file
					$appendFile = $rc;

				}

			}

		}

		// Second try: search under the templates.d subdirectory
		if($overrideFile === "") {

			// Override template file
			$rc = dirname($templateFile) . "/templates.d/" . basename($templateFile);

			// Test for the existence of an override file
			if(is_file($rc)) {

				Log::debug("Override template file: " . $rc);

				// Set override template file
				$overrideFile = $rc;

			}

		}

		// Third try: use the given template with no overrides
		if($overrideFile === "")
			$overrideFile = $templateFile;

		if($prependFile === "") {

			// Override prepend file
			$rc = dirname($templateFile) . "/templates.d/" . str_replace(".tpl", "_prepend.tpl", basename($templateFile));

			// Test for the existence of a prepend file
			if(is_file($rc)) {

				Log::debug("Override prepend file: " . $rc);

				// Set override prepend file
				$prependFile = $rc;

			}

		}

		if($appendFile === "") {

			// Override append file
			$rc = dirname($templateFile) . "/templates.d/" . str_replace(".tpl", "_append.tpl", basename($templateFile));

			// Test for the existence of an append file
			if(is_file($rc)) {

				Log::debug("Override append file: " . $rc);

				// Set override append file
				$appendFile = $rc;

			}

		}

		return array($prependFile, $overrideFile, $appendFile);

	}

	/**
	 * Save template configuration to file
	 * @param Smarty &$smarty Smarty object
	 * @param string $templateFile Template file which contains Smarty blocks
	 * @param string $configFile Full path and file name to save
	 * @param string $prepend Prepend extra string to the resulting template
	 * @param string $append Append extra string to the resulting template
	 * @param bool $modifiedOnly Overwrite on modifications, avoid issues with services that need daemon-reload
	 * @return bool
	 */
	protected function saveConfigFile(
		&$smarty,
		$templateFile,
		$configFile,
		$prepend = "",
		$append = "",
		$modifiedOnly = false
	) {

		// Template files
		list($prependFile, $templateFile, $appendFile) = $this->selectTemplateFile($templateFile, $smarty);

		Log::debug("Writing to file: " . $configFile);

		try {

			// Input validation
			if(!$smarty instanceof Smarty)
				return false;

			// Prepend
			if($prependFile !== "")
				$prepend = $prepend . $smarty->fetch($prependFile);

			// Append
			if($appendFile !== "")
				$append = $smarty->fetch($appendFile) . $append;

			// Generate new content based on all template files
			$new = $prepend . $smarty->fetch($templateFile) . $append;

		} catch(\Exception $e) {

			Log::error($e->getMessage());
			Log::error("Error while fetching template: " . $templateFile);
			return false;

		}

		// Overwrite file only when there are modifications
		if(
			$modifiedOnly === true &&
			is_file($configFile)
		) {

			// Load existing file
			$old = file_get_contents($configFile);

			// Compare and return early if contents match
			if($old === $new)
				return true;

		}

		// Save configuration file
		$rc = file_put_contents($configFile, $new, LOCK_EX);
		if($rc === false) {

			Log::error("Error while writing to file: " . $configFile);
			return false;

		}

		return true;

	}

	/**
	 * Set new or modify existing parameters within '.ini' files.
	 * @param string &$ini Ini file contents
	 * @param string $key Parameter key
	 * @param string $value Parameter value
	 * @param string $commentChar Default comment character is ';'
	 * @return bool
	 */
	protected function setIniParameter(&$ini, $key, $value, $commentChar = ";")
	{

		// Trim input
		$key = trim($key);
		$value = trim($value);

		// Changes counter
		$count = 0;

		/**
		 * The regex is clever enough to enable commented lines by removing the comment character.
		 *
		 * Regex analysis:
		 * 	^	start of line
		 *	;?	optional commented line character (default set to ';')
		 *	('key')	parameter key
		 *	\s?	optional whitespace
		 *	=	equal sign
		 *	\s?	optional whitespace
		 *	(.*)	parameter value
		 *	$	end of line
		 *
		 * PCRE modifiers:
		 *	U	PCRE_UNGREEDY
		 *	m	PCRE_MULTILINE
		 */
		$ini = preg_replace('/^' . $commentChar . '?(' . $key . ')\s?=\s?(.*)$/Um', '\1 = ' . $value, $ini, 1, $count);
		if($ini === null) {

			Log::error("Error while setting ini key " . $key . " with parameter: " . $value);
			return false;

		}

		// If the requested parameter was not found, append at the end
		if($count === 0)
			$ini .= "\n$key = $value\n";

		return true;

	}

	/**
	 * Set new or modify existing parameters within '.conf' files.
	 * @param string &$conf Conf file contents
	 * @param string $key Parameter key
	 * @param string $value Parameter value
	 * @param string $commentChar Default comment character is '#'
	 * @return bool
	 */
	protected function setConfParameter(&$conf, $key, $value, $commentChar = "#")
	{

		// Trim input
		$key = trim($key);
		$value = trim($value);

		// Changes counter
		$count = 0;

		/**
		 * The regex is clever enough to enable commented lines by removing the comment character.
		 *
		 * Regex analysis:
		 * 	^	start of line
		 *	#?	optional commented line character (default set to ';')
		 *	('key')	parameter key
		 *	\s	whitespace
		 *	(.*)	parameter value
		 *	$	end of line
		 *
		 * PCRE modifiers:
		 *	U	PCRE_UNGREEDY
		 *	m	PCRE_MULTILINE
		 */
		$conf = preg_replace('/^' . $commentChar . '?(' . $key . ')\s(.*)$/Um', '\1 ' . $value, $conf, 1, $count);
		if($conf === null) {

			Log::error("Error while setting configuration key " . $key . " with parameter: " . $value);
			return false;

		}

		// If the requested parameter was not found, append at the end
		if($count === 0)
			$conf .= "\n$key $value\n";

		return true;

	}

	/**
	 * Update .ini file per individual parameters
	 * @param string $templateFile Full path and file name to template file that contains individual parameters
	 * @param string $parameterFile Full path and file name to parameter file
	 * @param Smarty &$smarty Smarty object
	 * @return bool
	 */
	protected function updateIniFile($templateFile, $parameterFile, &$smarty)
	{

		try {

			// Load the individual parameters from template (ignore prepend/append files)
			list(, $paramsFile,) = $this->selectTemplateFile($templateFile);
			$params = explode("\n", $smarty->fetch($paramsFile));

		} catch(\Exception $e) {

			Log::error($e->getMessage());
			Log::error("Error while fetching template: " . $templateFile);
			return false;

		}

		// Load the parameter file into memory
		$data = file_get_contents($parameterFile);
		if($data === false) {

			Log::error("Error while loading file: " . $parameterFile);
			return false;

		}

		// Loop each parameter
		foreach($params as $p) {

			// Trim input
			$p = trim($p);

			// Skip empty, invalid or commented out lines
			if(
				$p == "" ||
				strpos($p, "=") === false ||
				strpos($p, "#") === 0 ||
				strpos($p, ";") === 0
			)
				continue;

			$p = explode("=", $p);

			$rc = $this->setIniParameter($data, $p[0], $p[1]);
			if($rc === false)
				return false;

		}

		// Save the modified parameter file
		Log::debug("Writing to file: " . $parameterFile);
		$rc = file_put_contents($parameterFile, $data, LOCK_EX);
		if($rc === false) {

			Log::error("Error while writing to file: " . $parameterFile);
			return false;

		}

		return true;

	}

	/**
	 * Export virtual host
	 * @param object $vhost Virtual host manager
	 * @param string $tdir Temporary export directory
	 * @param array $cmdParameters Optional command-line parameters
	 * @return bool
	 */
	public function export($vhost, $tdir, $cmdParameters = array())
	{

		// By default, modules do not export anything
		return true;

	}

}

