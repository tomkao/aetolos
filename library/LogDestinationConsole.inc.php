<?php
/**
 * Aetolos - Log destination to console implementation
 *
 * Display log events to console
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage logdestinationconsole
 */

/**
 * Log destination to console implementation class
 *
 * @package aetolos
 * @subpackage logdestinationconsole
 */
class LogDestinationConsole implements LogDestinationInterface {

	/**
	 * Selected output
	 * @var resource
	 */
	private $output = STDOUT;

	/**
	 * Constructor.
	 * @param string $output Select between normal (stdout) or error (stderr) output
	 * @return void
	 */
	public function __construct($output = "")
	{

		// Input validation
		switch($output) {
			case "stderr":
				// Set output for this log
				$this->output = STDERR;
				break;

			case "stdout":
			default:
				// Set output for this log
				$this->output = STDOUT;
				break;
		}

	}

	/**
	 * Write string to output
	 * @param string $string Message string
	 * @param int $priorityLevel Priority level
	 * @param array<string> $parameters Extra parameters to pass to the destination writer, in the form of an array of strings
	 * @return void
	 */
	public function write($string, $priorityLevel, $parameters = array())
	{

		// JSON
		if(in_array("json", $parameters)) {

			// Do not make changes to the string
			$parameters[] = "nobeautification";

			switch($priorityLevel) {

				case LOG_EMERG:
					$priorityLevelString = "EMERGENCY";
					break;

				case LOG_ALERT:
					$priorityLevelString = "ALERT";
					break;

				case LOG_CRIT:
					$priorityLevelString = "CRITICAL";
					break;

				case LOG_ERR:
					$priorityLevelString = "ERROR";
					break;

				case LOG_WARNING:
					$priorityLevelString = "WARNING";
					break;

				case LOG_NOTICE:
					$priorityLevelString = "NOTICE";
					break;

				case LOG_INFO:
					$priorityLevelString = "INFO";
					break;

				case LOG_DEBUG:
					$priorityLevelString = "DEBUG";
					break;

				default:
					$priorityLevelString = "";
					break;

			}

			// Generate a JSON encoded object
			$string = (string)json_encode(array(
				'source'	=> "aetolos",
				'priority'	=> $priorityLevelString,
				'message'	=> $string
			));

		}

		// Beautification
		if(!in_array("nobeautification", $parameters)) {

			switch($priorityLevel) {

				case LOG_EMERG:
					$string = "[" . chr(27) . "[41;1mEMERGENCY" . chr(27) . "[0m] " . $string;
					break;

				case LOG_ALERT:
					$string = "[" . chr(27) . "[41;1mALERT" . chr(27) . "[0m] " . $string;
					break;

				case LOG_CRIT:
					$string = "[" . chr(27) . "[45;1mCRITICAL" . chr(27) . "[0m] " . $string;
					break;

				case LOG_ERR:
					$string = "[" . chr(27) . "[31;1mERROR" . chr(27) . "[0m] " . $string;
					break;

				case LOG_WARNING:
					$string = "[" . chr(27) . "[33;1mWARNING" . chr(27) . "[0m] " . $string;
					break;

				case LOG_NOTICE:
					$string = "[" . chr(27) . "[34;1mNOTICE" . chr(27) . "[0m] " . $string;
					break;

				case LOG_INFO:
					$string = "[" . chr(27) . "[34;1mINFO" . chr(27) . "[0m] " . $string;
					break;

				case LOG_DEBUG:
					$string = "[" . chr(27) . "[34;1mDEBUG" . chr(27) . "[0m] " . $string;
					break;

			}

		}

		// Newline
		if(!in_array("nonewline", $parameters))
			$string .= "\n";

		// Output
		fwrite($this->output, $string);

	}

}

