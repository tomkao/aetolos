<?php
/**
 * Aetolos - Template factory for fake Smarty
 *
 * In order to remove the dependency on the Smarty library, a "fake" replacement was created
 * specifically for this purpose. The fake Smarty template engine is very limited in scope
 * as it supports only the commands used by Aetolos.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage templatefactory
 */

/**
 * Template factory class for Smarty
 *
 * @package aetolos
 * @subpackage templatefactory
 */
class TemplateFactory {

	/**
	 * Create a new template object
	 * @return Smarty
	 */
	public static function create()
	{

		// Fake Smarty
		return new Smarty();

	}

}

