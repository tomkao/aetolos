# Aetolos - Automatically generated Apache virtual host configuration

<VirtualHost *:80>

	ServerName {$SERVERNAME}
{if (isset($PREFIXALIAS) && $PREFIXALIAS!="") || (isset($PARKEDDOMAINS) && sizeof($PARKEDDOMAINS)>0)}
	ServerAlias{if isset($PREFIXALIAS) && $PREFIXALIAS!=""} {$PREFIXALIAS}.{$SERVERNAME}{/if}{if isset($PARKEDDOMAINS) && sizeof($PARKEDDOMAINS)>0}{foreach $PARKEDDOMAINS as $P} {$P->domainName}{if isset($P->prefixAlias) && $P->prefixAlias!=""} {$P->prefixAlias}.{$P->domainName}{/if}{/foreach}{/if}{/if}
	DocumentRoot {$HOME}/{$UNIXNAME}/public_html
	ServerAdmin webmaster@{$SERVERNAME}
	{if isset($MAXCLIENTS) && $MAXCLIENTS!=''}MaxClientsVHost {$MAXCLIENTS}{/if}
	Alias /.well-known /var/www/html/.well-known
	<Directory "/var/www/html/.well-known">
		Require all granted
		<LimitExcept GET HEAD>
			Require all denied
		</LimitExcept>
	</Directory>

	RewriteEngine On
	RewriteCond {literal}%{REQUEST_URI}{/literal} !^/\.well-known/acme-challenge/
	RewriteRule .* https://{literal}%{HTTP_HOST}%{REQUEST_URI}{/literal} [R=permanent,L]

</VirtualHost>

<VirtualHost *:443>

	ServerName {$SERVERNAME}
{if (isset($PREFIXALIAS) && $PREFIXALIAS!="") || (isset($PARKEDDOMAINS) && sizeof($PARKEDDOMAINS)>0)}
	ServerAlias{if isset($PREFIXALIAS) && $PREFIXALIAS!=""} {$PREFIXALIAS}.{$SERVERNAME}{/if}{if isset($PARKEDDOMAINS) && sizeof($PARKEDDOMAINS)>0}{foreach $PARKEDDOMAINS as $P} {$P->domainName}{if isset($P->prefixAlias) && $P->prefixAlias!=""} {$P->prefixAlias}.{$P->domainName}{/if}{/foreach}{/if}{/if}
	DocumentRoot {$HOME}/{$UNIXNAME}/public_html
	ServerAdmin webmaster@{$SERVERNAME}
	{if isset($MAXCLIENTS) && $MAXCLIENTS!=''}MaxClientsVHost {$MAXCLIENTS}{/if}
	SSLEngine on
	SSLCertificateFile {$CERTS}/{$SERVERNAME}.crt
	SSLCertificateKeyFile {$KEYS}/{$SERVERNAME}.key
	{if isset($CHAIN) && $CHAIN===true}SSLCertificateChainFile {$CERTS}/{$SERVERNAME}.chain
	{/if}
	{if isset($CABUNDLE) && $CABUNDLE===true}SSLCACertificateFile {$CERTS}/{$SERVERNAME}.cabundle
	{/if}
	ProxyRequests Off
	ProxyPreserveHost On
	RequestHeader set X-Forwarded-Proto "https"

        ProxyPass / http://127.0.0.1:{$PORT}/
        ProxyPassReverse / http://127.0.0.1:{$PORT}/

</VirtualHost>
{if isset($MTASTS, $PREFIXALIAS) && $MTASTS==="enabled" && $PREFIXALIAS!=""}
<VirtualHost *:443>

	ServerName mta-sts.{$SERVERNAME}
	DocumentRoot /var/www/html
	ServerAdmin webmaster@{$SERVERNAME}

	SSLEngine on
	SSLCertificateFile {$CERTS}/{$SERVERNAME}.crt
	SSLCertificateKeyFile {$KEYS}/{$SERVERNAME}.key
	{if isset($CHAIN) && $CHAIN===true}SSLCertificateChainFile {$CERTS}/{$SERVERNAME}.chain
	{/if}
	{if isset($CABUNDLE) && $CABUNDLE===true}SSLCACertificateFile {$CERTS}/{$SERVERNAME}.cabundle
	{/if}

	<Directory "/var/www/html/.well-known">
		Require all granted
		<LimitExcept GET HEAD>
			Require all denied
		</LimitExcept>
	</Directory>

	RewriteEngine On
	RewriteRule !^/.well-known/mta-sts.txt - [R=404,L]

</VirtualHost>
{/if}
