opcache.enable			= 1
opcache.memory_consumption	= 128
opcache.interned_strings_buffer	= 32
opcache.max_accelerated_files	= 4000
opcache.use_cwd			= 1
opcache.validate_timestamps	= 1
opcache.save_comments		= 0

