<?php
/**
 * Aetolos - PHP
 *
 * PHP support and implementation
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage php
 */

/**
 * Php class
 *
 * @package aetolos
 * @subpackage php
 */
class Php implements ModuleInterface {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Smarty template engine
	 * @var Smarty|null
	 */
	private $smarty = null;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"saveVirtualHosts",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'php'			=> "enabled",
			'php|iniFile'		=> "/etc/php.ini",
			'php|directoryFpmD'	=> "/etc/php-fpm.d",
			'php|opcacheFile'	=> "/etc/php.d/10-opcache.ini",
			'php|override'		=> "/etc/systemd/system/php-fpm.service.d/override.conf"
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		$rpm = array(
			"php",
			"php-cli",
			"php-common",
			"php-fpm",
			"php-intl",
			"php-mbstring",
			"php-mysqlnd",
			"php-pdo",
			"php-process",
			"php-soap",
			"php-xml",
			"php-opcache"
		);

		if(PHP_MAJOR_VERSION <= 7)
			$rpm[] = "php-json";

		return $rpm;

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array(),
			'service'	=> array("php-fpm"),
			'conflict'	=> array()
		);

	}

	/**
	 * Module command-line parameters
	 * @return array<string>
	 */
	public function cmdParameters()
	{

		return array();

	}

	/**
	 * Help
	 * @return array<string|array<string, string>>
	 */
	public function help()
	{

		return array();

	}

	/**
	 * Module controller
	 * @param array $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters)
	{

		return true;

	}

	/**
	 * Setup SELinux requirements
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts)
	{

		return true;

	}

	/**
	 * Save virtual hosts
	 * @return bool
	 */
	public function saveVirtualHosts()
	{

		// Smarty template
		if($this->smarty === null)
			$this->smarty = TemplateFactory::create();

		// Get all virtual hosts and generate individual configurations
		$vhosts = VirtualHostFactory::populate();

		// Generate virtual hosts
		foreach($vhosts as &$v) {

			// Skip parked domains
			if($v->parkedUnder != "")
				continue;

			// Assign variables
			$this->smarty->assignByRef("SERVERNAME", $v->domainName);
			$this->smarty->assignByRef("UNIXNAME", $v->unixName);
			$this->smarty->assignByRef("HOME", $v->home);

			// Save configuration file
			$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/virtualhostfpm.tpl", Config::read("php|directoryFpmD") . "/" . $v->domainName . ".conf");
			if($rc === false)
				return false;

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: php");

		// Smarty template
		$this->smarty = TemplateFactory::create();

		// Required directory for php-fpm systemd directory
		if(!is_dir("/etc/systemd/system/php-fpm.service.d")) {

			// Create php-fpm.service.d directory for systemd variables
			$rc = mkdir("/etc/systemd/system/php-fpm.service.d", 0755);
			if($rc === false)
				return false;

			// Request systemd reload
			$this->daemonReload = true;

		}

		// Generate override.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/override.tpl", Config::read("php|override"), "", "", true);
		if($rc === false)
			return false;

		// Disable default www pool
		$file = Config::read("php|directoryFpmD") . "/www.conf";
		if(is_file($file) && filesize($file) > 0) {
			rename($file, $file . ".disabled");
			touch($file);
		}

		// Try to detect the timezone of the server
		$timezone = "";
		$timedatectl = array();
		exec("/usr/bin/timedatectl", $timedatectl);
		foreach($timedatectl as $t) {

			// Look for time zone string
			if(strpos($t, "Time zone") !== false) {

				$rc = preg_match('/Time zone: (.*) \(/U', $t, $matches);
				if($rc === 1 && isset($matches[1])) {

					// Match found, store and break from loop
					$timezone = trim($matches[1]);
					break;

				}

			}

		}

		// Assign variables
		$this->smarty->assignByRef("TIMEZONE", $timezone);

		$rc = $this->updateIniFile(__DIR__ . "/php.tpl", Config::read("php|iniFile"), $this->smarty);
		if($rc === false)
			return false;

		$rc = $this->updateIniFile(__DIR__ . "/opcache.tpl", Config::read("php|opcacheFile"), $this->smarty);
		if($rc === false)
			return false;

		// Get all virtual hosts and generate individual configurations
		$rc = $this->saveVirtualHosts();
		if($rc === false)
			return false;

		return true;

	}

	/**
	 * Disable module
	 * @return bool
	 */
	public function disable()
	{

		Log::warning("Disable not implemented or not applicable for module: php");

		return false;

	}

}

