# Aetolos - Automatically generated Dovecot configuration

default_process_limit = 50
default_client_limit = 500
service imap-login {
	inet_listener imap {
	{if isset($ROUNDCUBE) && $ROUNDCUBE==="enabled"}
		address = localhost
		port = 143
	{else}
		port = 0
	{/if}
	}
	inet_listener imaps {
		port = 993
		ssl = yes
	}
	service_count = 1000
}
service pop3-login {
	inet_listener pop3 {
		port = 0
	}
	inet_listener pop3s {
		port = 995
		ssl = yes
	}
}
service imap {
	vsz_limit = 1024MB
}
service pop3 {
}
service lmtp {
	unix_listener /var/spool/postfix/private/dovecot-lmtp {
		mode = 0660
		user = postfix
		group = postfix
	}
}
service auth {
	unix_listener /var/spool/postfix/private/auth {
		mode = 0660
		user = postfix
		group = postfix
	}
	user = root
}
service auth-worker {
	user = root
}
service indexer-worker {
	vsz_limit = 0
}

