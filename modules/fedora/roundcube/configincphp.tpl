// Aetolos - Automatically generated Roundcube configuration

$config = array();
$config['db_dsnw'] = 'mysql://{$USER}:{$PASSWORD}@localhost/roundcubemail';
{if isset($RVERSION) && $RVERSION=='OLD'}
$config['default_host'] = 'localhost';
$config['default_port'] = 143;
$config['smtp_server'] = 'tls://localhost';
$config['smtp_port'] = 587;
{else}
$config['imap_host'] = 'localhost:143';
$config['smtp_host'] = 'tls://localhost:587';
{/if}
$config['smtp_conn_options']['localhost'] = [ 'ssl' => [ 'verify_peer' => false, 'verify_peer_name' => false ] ];
$config['smtp_user'] = '%u';
$config['smtp_pass'] = '%p';
$config['support_url'] = '';
$config['product_name'] = '';
$config['des_key'] = '{$DESKEY}';
$config['plugins'] = array('archive');
$config['skin'] = 'elastic';
$config['log_driver'] = 'syslog';
$config['syslog_facility'] = LOG_MAIL;
$config['smtp_log'] = false;
$config['imap_vendor'] = 'dovecot';
$config['log_dir'] = '/var/log/roundcubemail/';
$config['temp_dir'] = '/var/lib/roundcubemail/temp/';
$config['force_https'] = true;
$config['login_autocomplete'] = 2;
$config['display_product_info'] = 0;
$config['session_lifetime'] = 60;
$config['trusted_host_patterns'] = array();
$config['ip_check'] = true;
$config['cipher_method'] = 'AES-256-CBC';
$config['username_domain'] = '%t';
$config['username_domain_forced'] = true;
$config['mail_domain'] = '%t';
$config['max_message_size'] = '50M';
$config['max_recipients'] = 100;
$config['useragent'] = '';
$config['line_length'] = 80;
$config['identities_level'] = 3;
$config['identity_image_size'] = 512;
$config['mime_types'] = '/etc/mime.types';
$config['message_sort_col'] = 'date';
$config['date_format'] = 'd-m-Y';
$config['date_long'] = 'd-m-Y H:i';
$config['create_default_folders'] = true;
$config['enable_spellcheck'] = false;
$config['default_charset'] = 'UTF-8';
$config['show_images'] = 1;
$config['htmleditor'] = 4;
//$config['skip_deleted'] = true;
$config['check_all_folders'] = true;
$config['display_next'] = false;
$config['reply_mode'] = 1;
$config['message_show_email'] = true;
$config['reply_all_mode'] = 1;
$config['skin_logo'] = null;
$config['x_frame_options'] = false;
$config['include_host_config'] = true;

