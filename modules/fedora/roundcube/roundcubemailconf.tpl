# Aetolos - Automatically generated Roundcube configuration

Alias /webmail /usr/share/roundcubemail
<Directory /usr/share/roundcubemail/>
	AllowOverride All
	Require all granted
        <IfModule mod_php5.c>
		AssignUserID apache apache
		php_admin_value open_basedir "/usr/share/roundcubemail/:/etc/roundcubemail/:/var/log/roundcubemail/:/var/lib/roundcubemail/:/usr/lib/php/:/usr/lib64/php/:/usr/share/php/:/usr/local/lib/php/:/usr/local/lib64/php/:/tmp/:/usr/share/pear/:/usr/share/pear-data/"
	</IfModule>
	<IfModule !mod_php5.c>
		<IfModule !mod_php7.c>
			{literal}<If "%{REQUEST_URI} =~ /\.php$/ && -f %{REQUEST_FILENAME}" >{/literal}
				SetHandler "proxy:unix:/run/php-fpm/roundcube.sock|fcgi://localhost"
			</If>
		</IfModule>
	</IfModule>
	<IfModule mod_headers.c>
		Header always set X-Frame-Options "SAMEORIGIN"
	</IfModule>
</Directory>
<Directory /usr/share/roundcubemail/installer/>
	Require all denied
</Directory>
<Directory /usr/share/roundcubemail/bin/>
	Require all denied
</Directory>
<Directory /usr/share/roundcubemail/plugins/enigma/home/>
	Require all denied
</Directory>
<Directory /usr/share/roundcubemail/SQL/>
	Require all denied
</Directory>

