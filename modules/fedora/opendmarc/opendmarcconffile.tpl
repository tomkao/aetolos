# Aetolos - Automatically generated OpenDMARC configuration

BaseDirectory		/run/opendmarc
PidFile			/run/opendmarc/opendmarc.pid
PublicSuffixList	/usr/share/publicsuffix/public_suffix_list.dat
RejectFailures		true
Socket			inet:8893@localhost
SoftwareHeader		false
SPFIgnoreResults	true
SPFSelfValidate		true
Syslog			true
UMask			007
UserID			opendmarc:mail

