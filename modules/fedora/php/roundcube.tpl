[roundcube]
user			= apache
group			= apache

listen			= /run/php-fpm/roundcube.sock
listen.mode		= 0666

pm			= dynamic
pm.max_requests		= 10000

pm.start_servers	= 1
pm.min_spare_servers	= 1
pm.max_spare_servers	= 10
pm.max_children		= 10

security.limit_extensions = .php

php_admin_value[open_basedir] = "/usr/lib/php/:/usr/lib64/php/:/usr/share/php/:/usr/local/lib/php/:/usr/local/lib64/php/:/tmp/:/usr/share/pear/:/usr/share/pear-data/:/usr/share/GeoIP/:/usr/share/roundcubemail/:/etc/roundcubemail/:/var/log/roundcubemail/:/var/lib/roundcubemail/"

