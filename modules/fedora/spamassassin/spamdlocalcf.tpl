# Aetolos - Automatically generated SpamAssassin configuration

# General
required_score 5.0
use_bayes 1
dns_available yes
skip_rbl_checks 0
rewrite_header Subject [SPAM]

# Raise score for Bayes
score BAYES_80 4.5
score BAYES_95 5.0
score BAYES_99 10.0
score BAYES_999 10.0

# Raise score for DNS blacklists
score RCVD_IN_BRBL_LASTEXT 10
score RCVD_IN_RP_RNBL 10
score RCVD_IN_SBL 10
score RCVD_IN_XBL 10
score RCVD_IN_PBL 10
score RCVD_IN_PSBL 10
score RCVD_IN_SORBS_DUL 4
score RCVD_IN_SORBS_WEB 10
score RCVD_IN_SORBS_SMTP 10
score RCVD_IN_SORBS_SPAM 4

# Raise score for URI blacklists
score URIBL_SBL 10
score URIBL_PH_SURBL 10
score URIBL_WS_SURBL 10
score URIBL_MW_SURBL 10
score URIBL_CR_SURBL 10
score URIBL_ABUSE_SURBL 10
score URIBL_BLACK 10
score URIBL_RED 3
score URIBL_GREY 2

# Raise score for URI phishing forms
score URI_PHISH 10

# Raise score for obscuring source via google proxy
score URI_GOOGLE_PROXY 10

# Disable blacklists used by postfix and spammy "white" lists
dns_query_restriction deny bl.spamcop.net zen.spamhaus.org dbl.spamhaus.org psbl.surriel.com dnsbl.sorbs.net multi.uribl.com list.dnswl.org wl.mailspike.net iadb.isipp.com

#score RCVD_IN_BL_SPAMCOP_NET 10
#score URIBL_DBL_SPAM 10
#score URIBL_DBL_PHISH 5

{if isset($OPENDMARC) && $OPENDMARC=='enabled'}
header   AUTHRES_SPF_FAIL  Authentication-Results =~ /{$SERVERNAMEREGEX}; spf=fail/
score    AUTHRES_SPF_FAIL  50.0
describe AUTHRES_SPF_FAIL  Authentication results SPF fail

header   AUTHRES_DMARC_FAIL  Authentication-Results =~ /{$SERVERNAMEREGEX}; dmarc=fail/
score    AUTHRES_DMARC_FAIL  50.0
describe AUTHRES_DMARC_FAIL  Authentication results DMARC fail
{/if}

{if isset($SPFSA) && $SPFSA==true}
score SPF_FAIL 10
score SPF_SOFTFAIL 10
score SPF_HELO_FAIL 10
score SPF_HELO_SOFTFAIL 10
{else}
# Disable - use postfix for SPF checks
#score SPF_FAIL 10
#score SPF_SOFTFAIL 10
#score SPF_HELO_FAIL 10
#score SPF_HELO_SOFTFAIL 10
{/if}

# Raise score for failed DKIM tests
score DKIM_INVALID 10
score T_DKIM_INVALID 10
score DKIM_ADSP_ALL 10
score DKIM_ADSP_DISCARD 100
score DKIM_ADSP_NXDOMAIN 10
score DKIM_ADSP_CUSTOM_HIGH 5
score DKIM_ADSP_CUSTOM_MED 4
score NML_ADSP_CUSTOM_HIGH 5
score NML_ADSP_CUSTOM_MED 4

# Raise score of forged Reply-To emails
score FREEMAIL_FORGED_REPLYTO 5

# Remove sender domain matches handover relay domain
score RP_MATCHES_RCVD 0

# Fix some emails with all caps
score SUBJ_ALL_CAPS 0

# Raise score for broken emails
score UNPARSEABLE_RELAY 5

