[{$UNIXNAME}]
user			= {$UNIXNAME}
group			= {$UNIXNAME}

listen			= /run/php-fpm/{$UNIXNAME}.sock
listen.mode		= 0660
listen.acl_users	= {$UNIXNAME}

pm			= dynamic
pm.max_requests		= 100000

; small server
; ------------
pm.start_servers	= 5
pm.min_spare_servers	= 5
pm.max_spare_servers	= 25
pm.max_children		= 50

; medium server
; -------------
;pm.start_servers	= 15
;pm.min_spare_servers	= 15
;pm.max_spare_servers	= 50
;pm.max_children	= 100

; large-ish server
; ----------------
;pm.start_servers	= 30
;pm.min_spare_servers	= 30
;pm.max_spare_servers	= 100
;pm.max_children	= 200

security.limit_extensions =

php_admin_value[open_basedir]		= "{$HOME}/{$UNIXNAME}/:/usr/lib/php/:/usr/lib64/php/:/usr/share/php/:/usr/local/lib/php/:/usr/local/lib64/php/:/tmp/:/usr/share/pear/:/usr/share/pear-data/:/usr/share/GeoIP/"
php_admin_value[upload_tmp_dir]		= "{$HOME}/{$UNIXNAME}/tmp"
php_admin_value[session.save_handler]	= files
php_admin_value[session.save_path]	= "{$HOME}/{$UNIXNAME}/tmp"
php_admin_value[sys_temp_dir]		= "{$HOME}/{$UNIXNAME}/tmp"
php_admin_value[soap.wsdl_cache_dir]	= "{$HOME}/{$UNIXNAME}/tmp"
php_admin_value[opcache.file_cache]	= "{$HOME}/{$UNIXNAME}/tmp"

