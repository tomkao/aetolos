# Aetolos - Automatically generated Apache configuration

Listen					443 https
SSLPassPhraseDialog			exec:/usr/libexec/httpd-ssl-pass-dialog
SSLSessionCache				shmcb:/run/httpd/sslcache(1024000)
SSLSessionCacheTimeout			600
SSLRandomSeed				startup file:/dev/urandom  256
SSLRandomSeed				connect builtin
SSLCryptoDevice				builtin
SSLProtocol				all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
SSLProxyProtocol			all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
SSLHonorCipherOrder			on
SSLCipherSuite				PROFILE=SYSTEM
SSLProxyCipherSuite			PROFILE=SYSTEM
SSLCompression				off
SSLStrictSNIVHostCheck			on
SSLSessionTickets			off

# Stapling requires the web server to perform outgoing connections to retrieve data! Disabled by default.
SSLUseStapling				off
SSLStaplingResponderTimeout		5
SSLStaplingReturnResponderErrors	off
SSLStaplingCache			shmcb:/run/httpd/ocsp(512000)

