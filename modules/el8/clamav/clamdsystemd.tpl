[Unit]
Description = clamd scanner daemon
After = syslog.target nss-lookup.target network.target

[Service]
Type = simple
ExecStart = /usr/sbin/clamd -c {$CLAMDCONFFILE}
Restart = on-failure
PrivateTmp = true

[Install]
WantedBy=multi-user.target

