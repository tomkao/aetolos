<?php
/**
 * Aetolos - PHP
 *
 * PHP support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage php
 */

/**
 * Php class
 *
 * @package aetolos
 * @subpackage php
 */
class Php implements ModuleInterface {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'php'			=> "enabled",
			'php|iniFile'		=> "/etc/php.ini",
			'php|opcacheFile'	=> "/etc/php.d/opcache.ini"
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"php",
			"php-cli",
			"php-common",
			"php-gd",
			"php-intl",
			"php-ldap",
			"php-mbstring",
			"php-mysqlnd",
			"php-pdo",
			"php-process",
			"php-pspell",
			"php-soap",
			"php-xml",
			"php-mcrypt",
			"php-pecl-zendopcache"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array(),
			'service'	=> array(),
			'conflict'	=> array(
				"php72u-cli",
				"php72u-common",
				"mod_php72u"
			)
		);

	}

	/**
	 * Module command-line parameters
	 * @return array<string>
	 */
	public function cmdParameters()
	{

		return array();

	}

	/**
	 * Help
	 * @return array<string|array<string, string>>
	 */
	public function help()
	{

		return array();

	}

	/**
	 * Module controller
	 * @param array $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters)
	{

		return true;

	}

	/**
	 * Setup SELinux requirements
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts)
	{

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: php");

		// Smarty template
		$smarty = TemplateFactory::create();

		// Try to detect the timezone of the server
		$timezone = "";
		$timedatectl = array();
		exec("/usr/bin/timedatectl", $timedatectl);
		foreach($timedatectl as $t) {

			// Look for time zone string
			if(strpos($t, "Time zone") !== false) {

				$rc = preg_match('/Time zone: (.*) \(/U', $t, $matches);
				if($rc === 1 && isset($matches[1])) {

					// Match found, store and break from loop
					$timezone = trim($matches[1]);
					break;

				}

			}

		}

		// Assign variables
		$smarty->assignByRef("TIMEZONE", $timezone);

		$rc = $this->updateIniFile(__DIR__ . "/php.tpl", Config::read("php|iniFile"), $smarty);
		if($rc === false)
			return false;

		$rc = $this->updateIniFile(__DIR__ . "/opcache.tpl", Config::read("php|opcacheFile"), $smarty);
		if($rc === false)
			return false;

		return true;

	}

	/**
	 * Enable module
	 * @return bool
	 */
	public function enable()
	{

		Log::debug("Enabling module: php");

		// Configuration
		Config::write("php", "enabled");
		Config::write("php72us", "disabled");

		// Switch to apache if apache24us is enabled
		if(Config::read("apache24us") === "enabled") {

			Config::write("apache24us", "disabled");
			Config::write("apache", "enabled");

		}

		// SystemD controller
		$this->systemCtl("enable");

		return true;

	}

	/**
	 * Disable module
	 * @return bool
	 */
	public function disable()
	{

		Log::warning("Disable not implemented or not applicable for module: php");

		return false;

	}

	/**
	 * Start service
	 * @return bool
	 */
	public function start()
	{

		Log::warning("Start not implemented or not applicable for module: php");

		return false;

	}

	/**
	 * Stop service
	 * @return bool
	 */
	public function stop()
	{

		Log::warning("Stop not implemented or not applicable for module: php");

		return false;

	}

	/**
	 * Reload service
	 * @return bool
	 */
	public function reload()
	{

		Log::warning("Reload not implemented or not applicable for module: php");

		return false;

	}

	/**
	 * Service status
	 * @return string
	 */
	public function status()
	{

		return "";

	}

}

