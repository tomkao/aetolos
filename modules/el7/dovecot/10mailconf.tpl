# Aetolos - Automatically generated Dovecot configuration

mail_home = /etc/dovecot/vhost/%d
mail_location = maildir:~/
namespace {
	type = private
	prefix = INBOX.
	inbox = yes
}
first_valid_uid = 1000
mail_plugins = zlib quota
maildir_copy_with_hardlinks = yes
maildir_very_dirty_syncs = yes
mbox_write_locks = fcntl

