<?php
/**
 * Aetolos - Dovecot
 *
 * Dovecot support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage dovecot
 */

/**
 * Dovecot class
 *
 * @package aetolos
 * @subpackage dovecot
 */
class Dovecot implements ModuleInterface {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Smarty template engine
	 * @var Smarty|null
	 */
	private $smarty = null;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"cmdParameters",
			"help",
			"controller",
			"selinux",
			"saveVirtualHosts",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'dovecot'			=> "disabled",
			'dovecot|confFile'		=> "/etc/dovecot/dovecot.conf",
			'dovecot|directoryConfD'	=> "/etc/dovecot/conf.d",
			'dovecot|selinuxModule'		=> ""
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array("dovecot");

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array(),
			'service'	=> array("dovecot"),
			'conflict'	=> array()
		);

	}

	/**
	 * Module command-line parameters
	 * @return array<string>
	 */
	public function cmdParameters()
	{

		return array(
			"virtualhost:",
			"add-email:",
			"remove-email:",
			"modify-email:",
			"list-emails",
			"password:",
			"password-file:",
			"quota:",
			"enable",
			"disable"
		);

	}

	/**
	 * Help
	 * @return array<string|array<string, string>>
	 */
	public function help()
	{

		return array(
			"  --module=dovecot",
			array('short' => "",	'long' => "virtualhost=[DOMAIN]",		'desc' => "Select the virtual host to modify with the following commands"),
			array('short' => "",	'long2' => "add-email=[EMAIL]",			'desc' => "Add new email address (the @domain part is optional)"),
			array('short' => "",	'long3' => "password=[PASSWORD]",		'desc' => "Set email password (plain text or encrypted)"),
			array('short' => "",	'long3' => "password-file=[FILE]",		'desc' => "Set email password from file (plain text or encrypted)"),
			array('short' => "",	'long3' => "quota=[bytes]",			'desc' => "Set email quota (in bytes)"),
			"",
			array('short' => "",	'long2' => "remove-email=[EMAIL]",		'desc' => "Remove an email address and delete all email data"),
			"",
			array('short' => "",	'long2' => "modify-email=[EMAIL]",		'desc' => "Select an email address to modify"),
			array('short' => "",	'long3' => "password=[PASSWORD]",		'desc' => "Set email password (plain text or encrypted)"),
			array('short' => "",	'long3' => "password-file=[FILE]",		'desc' => "Set email password from file (plain text or encrypted)"),
			array('short' => "",	'long3' => "quota=[bytes]",			'desc' => "Set email quota (in bytes)"),
			array('short' => "",	'long3' => "enable",				'desc' => "Enable email address"),
			array('short' => "",	'long3' => "disable",				'desc' => "Disable email address"),
			"",
			array('short' => "",	'long2' => "list-emails",			'desc' => "List all emails for the selected virtual host"),
			"  EMAIL = the @domain part is optional, if none given then the domain is selected from the --virtualhost parameter",
			"  PASSWORD = encrypted passwords must have the {MD5-CRYPT} or {SHA512-CRYPT} prefix, otherwise they will be parsed as plain text.",
			""
		);

	}

	/**
	 * Module controller
	 * @param array $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters)
	{

		// Check for parameter
		if(!isset($cmdParameters['virtualhost'])) {

			Log::error("Missing parameter");
			return false;

		}

		$vhost = new VirtualHostManager();
		$vhost->domainName = $cmdParameters['virtualhost'];
		$rc = $vhost->get();
		if($rc === false) {

			Log::error("Unknown virtual host");
			return false;

		}

		$emailManager = new EmailManager($vhost);

		if(isset($cmdParameters['add-email'])) {

			if(!isset($cmdParameters['password'])) {

				if(isset($cmdParameters['password-file'])) {

					if(!is_file($cmdParameters['password-file'])) {

						Log::error("Error while opening the password file");
						return false;

					}

					$cmdParameters['password'] = file_get_contents($cmdParameters['password-file']);
					if($cmdParameters['password'] === false) {

						Log::error("Error while reading the password file");
						return false;

					}

					// Remove newlines
					$cmdParameters['password'] = str_replace("\n", "", $cmdParameters['password']);

				} else {

					Log::error("Password not defined");
					return false;

				}

			}

			if(!isset($cmdParameters['quota']))
				$cmdParameters['quota'] = 0;

			if(!is_numeric($cmdParameters['quota'])) {

				Log::error("Quota must be a numeric integer");
				return false;

			}

			$rc = $emailManager->add($cmdParameters['add-email'], $cmdParameters['password'], (int)$cmdParameters['quota']);
			if($rc === false) {

				Log::error("Error while creating new email address");
				return false;

			}

		} elseif(isset($cmdParameters['remove-email'])) {

			$rc = $emailManager->remove($cmdParameters['remove-email']);
			if($rc === false) {

				Log::error("Error while removing email address");
				return false;

			}

		} elseif(isset($cmdParameters['modify-email'])) {

			if(!isset($cmdParameters['password']) && isset($cmdParameters['password-file'])) {

				if(!is_file($cmdParameters['password-file'])) {

					Log::error("Error while opening the password file");
					return false;

				}

				$cmdParameters['password'] = file_get_contents($cmdParameters['password-file']);
				if($cmdParameters['password'] === false) {

					Log::error("Error while reading the password file");
					return false;

				}

				// Remove newlines
				$cmdParameters['password'] = str_replace("\n", "", $cmdParameters['password']);

			}

			if(isset($cmdParameters['password'])) {

				$rc = $emailManager->setPassword($cmdParameters['modify-email'], $cmdParameters['password']);
				if($rc === false) {

					Log::error("Error while setting email password");
					return false;

				}

			}

			if(isset($cmdParameters['quota'])) {

				if(!is_numeric($cmdParameters['quota'])) {

					Log::error("Quota must be a numeric integer");
					return false;

				}

				$rc = $emailManager->setQuota($cmdParameters['modify-email'], (int)$cmdParameters['quota']);
				if($rc === false) {

					Log::error("Error while setting email quota");
					return false;

				}

			}

			if(isset($cmdParameters['enable'])) {

				$rc = $emailManager->enable($cmdParameters['modify-email']);
				if($rc === false) {

					Log::error("Error while enabling email address");
					return false;

				}

			} elseif(isset($cmdParameters['disable'])) {

				$rc = $emailManager->disable($cmdParameters['modify-email']);
				if($rc === false) {

					Log::error("Error while disabling email address");
					return false;

				}

			}

		} elseif(isset($cmdParameters['list-emails'])) {

			$data = $emailManager->listEmails();

			// JSON output
			if(Config::read("json") === true) {

				if(Config::read("daemon") === true)
					// Return data to the daemon
					return $data;
				else
					// Generate a JSON encoded object
					echo json_encode(array('Emails' => $data));

			} else {

				// Columns
				$columns = array(
					array(
						'header'	=> "Email",
						'arrayKey'	=> 0,
						'maxSize'	=> 6
					),
					array(
						'header'	=> "Quota",
						'arrayKey'	=> 1,
						'maxSize'	=> 6
					),
					array(
						'header'	=> "Usage",
						'arrayKey'	=> 2,
						'maxSize'	=> 6
					),
					array(
						'header'	=> "Last password change",
						'arrayKey'	=> 3,
						'maxSize'	=> 21
					)
				);

				Ascii::drawTable($columns, $data);

			}

		} else {

			Log::error("Parameter error");
			return false;

		}

		return true;

	}

	/**
	 * Setup SELinux requirements
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts)
	{

		// SELinux - allow Dovecot to access user home directories to read passwd/shadow files under user/etc
		if(Config::read("dovecot|selinuxModule") !== "installed13") {

			Log::debug("SELinux: enable dovecot_home_etc_aetolos.pp");

			// Install SELinux module
			exec("/usr/sbin/semodule -i " . __DIR__ . "/dovecot_home_etc_aetolos.pp 2>/dev/null");

			// Remember the module has already been installed
			Config::write("dovecot|selinuxModule", "installed13");

		}

		return true;

	}

	/**
	 * Save virtual hosts
	 * @return bool
	 */
	public function saveVirtualHosts()
	{

		// Smarty template
		if($this->smarty === null)
			$this->smarty = TemplateFactory::create();

		// Get all virtual hosts and generate SNI configurations
		$vhosts = VirtualHostFactory::populate();

		// Assign variables
		$this->smarty->assignByRef("VHOSTS", $vhosts);
		$this->smarty->assign("DEHYDRATED", Config::read("dehydrated"));

		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/10sslconf.tpl", Config::read("dovecot|directoryConfD") . "/10-ssl.conf");
		if($rc === false)
			return false;
		else
			return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: dovecot");

		// Smarty template
		if($this->smarty === null)
			$this->smarty = TemplateFactory::create();

		// Required directories for virtual hosts
		if(!is_dir("/etc/dovecot/vhost")) {

			// Create vhost directory for storing passwd/shadow files
			$rc = mkdir("/etc/dovecot/vhost", 0700);
			if($rc === false)
				return false;

		}

		// Assign variables
		$this->smarty->assign("ROUNDCUBE", Config::read("roundcube", true));

		// Generate dovecot.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/dovecotconf.tpl", Config::read("dovecot|confFile"));
		if($rc === false)
			return false;

		// Generate 10-auth.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/10authconf.tpl", Config::read("dovecot|directoryConfD") . "/10-auth.conf");
		if($rc === false)
			return false;

		// Generate 10-mail.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/10mailconf.tpl", Config::read("dovecot|directoryConfD") . "/10-mail.conf");
		if($rc === false)
			return false;

		// Generate 10-master.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/10masterconf.tpl", Config::read("dovecot|directoryConfD") . "/10-master.conf");
		if($rc === false)
			return false;

		// Generate 10-ssl.conf and save virtual hosts
		$rc = $this->saveVirtualHosts();
		if($rc === false)
			return false;

		// Generate 15-lda.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/15ldaconf.tpl", Config::read("dovecot|directoryConfD") . "/15-lda.conf");
		if($rc === false)
			return false;

		// Generate 15-mailboxes.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/15mailboxesconf.tpl", Config::read("dovecot|directoryConfD") . "/15-mailboxes.conf");
		if($rc === false)
			return false;

		// Generate 20-imap.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/20imapconf.tpl", Config::read("dovecot|directoryConfD") . "/20-imap.conf");
		if($rc === false)
			return false;

		// Generate 20-pop3.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/20pop3conf.tpl", Config::read("dovecot|directoryConfD") . "/20-pop3.conf");
		if($rc === false)
			return false;

		// Generate 90-acl.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/90aclconf.tpl", Config::read("dovecot|directoryConfD") . "/90-acl.conf");
		if($rc === false)
			return false;

		// Generate 90-quota.conf
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/90quotaconf.tpl", Config::read("dovecot|directoryConfD") . "/90-quota.conf");
		if($rc === false)
			return false;

		// Generate auth-passwdfile.conf.ext
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/authpasswdconfext.tpl", Config::read("dovecot|directoryConfD") . "/auth-passwdfile.conf.ext");
		if($rc === false)
			return false;
		else
			return true;

	}

}

