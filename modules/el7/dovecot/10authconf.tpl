# Aetolos - Automatically generated Dovecot configuration

disable_plaintext_auth = yes
auth_cache_size = 1024
auth_username_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#$-=?^_{}~.@+%"
auth_mechanisms = plain login
!include auth-passwdfile.conf.ext

