# Aetolos - Automatically generated MariaDB configuration

[server]
bind-address=127.0.0.1

[mysqld]

# common parameters
# -----------------
query_cache_type=1
max_allowed_packet=16777216
group_concat_max_len=1048576
default-storage-engine=innodb
thread_cache_size=7
innodb_buffer_pool_instances=1
innodb_flush_log_at_trx_commit=2
innodb_flush_method=O_DIRECT
innodb_file_per_table
innodb_file_format=barracuda
innodb_stats_on_metadata=off
collation-server = utf8mb4_unicode_ci
character-set-server = utf8mb4
wait_timeout = 3600
interactive_timeout = 60

# per-thread memory
# -----------------
{foreach $PERTHREADMEMORY as $key => $P}{$key}={$P}
{/foreach}

# server buffers
# --------------
{foreach $SERVERBUFFERS as $key => $S}{$key}={$S}
{/foreach}

