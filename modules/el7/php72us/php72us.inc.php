<?php
/**
 * Aetolos - PHP 7.2 IUS
 *
 * PHP 7.2 IUS support and implementation
 *
 * @copyright Noumenia (C) 2018 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage php72us
 */

/**
 * Php72us class
 *
 * @package aetolos
 * @subpackage php72us
 */
class Php72us implements ModuleInterface {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Smarty template engine
	 * @var Smarty|null
	 */
	private $smarty = null;

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"saveVirtualHosts",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'php72us'		=> "disabled",
			'php72us|iniFile'	=> "/etc/php.ini",
			'php72us|directoryFpmD'	=> "/etc/php-fpm.d",
			'php72us|opcacheFile'	=> "/etc/php.d/10-opcache.ini"
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"php72u-cli",
			"php72u-common",
			"php72u-fpm",
			"php72u-gd",
			"php72u-intl",
			"php72u-ldap",
			"php72u-mbstring",
			"php72u-mysqlnd",
			"php72u-pdo",
			"php72u-process",
			"php72u-pspell",
			"php72u-soap",
			"php72u-xml",
			"php72u-json",
			"php72u-opcache",
			"mod_php72u"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array("ius-release"),
			'service'	=> array("php-fpm"),
			'conflict'	=> array(
				"php-cli",
				"php-common"
			)
		);

	}

	/**
	 * Module command-line parameters
	 * @return array<string>
	 */
	public function cmdParameters()
	{

		return array();

	}

	/**
	 * Help
	 * @return array<string|array<string, string>>
	 */
	public function help()
	{

		return array();

	}

	/**
	 * Module controller
	 * @param array $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters)
	{

		return true;

	}

	/**
	 * Setup SELinux requirements
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts)
	{

		return true;

	}

	/**
	 * Save virtual hosts
	 * @return bool
	 */
	public function saveVirtualHosts()
	{

		// Smarty template
		if($this->smarty === null)
			$this->smarty = TemplateFactory::create();

		// Get all virtual hosts and generate individual configurations
		$vhosts = VirtualHostFactory::populate();

		// Generate virtual hosts
		foreach($vhosts as &$v) {

			// Skip parked domains
			if($v->parkedUnder != "")
				continue;

			// Assign variables
			$this->smarty->assignByRef("SERVERNAME", $v->domainName);
			$this->smarty->assignByRef("UNIXNAME", $v->unixName);
			$this->smarty->assignByRef("HOME", $v->home);

			// Save configuration file
			$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/virtualhostfpm.tpl", Config::read("php72us|directoryFpmD") . "/" . $v->domainName . ".conf");
			if($rc === false)
				return false;

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: php72us");

		// Smarty template
		$this->smarty = TemplateFactory::create();

		// Disable default www pool
		$file = Config::read("php72us|directoryFpmD") . "/www.conf";
		if(is_file($file) && filesize($file) > 0) {
			rename($file, $file . ".disabled");
			touch($file);
		}

		// Try to detect the timezone of the server
		$timezone = "";
		$timedatectl = array();
		exec("/usr/bin/timedatectl", $timedatectl);
		foreach($timedatectl as $t) {

			// Look for time zone string
			if(strpos($t, "Time zone") !== false) {

				$rc = preg_match('/Time zone: (.*) \(/U', $t, $matches);
				if($rc === 1 && isset($matches[1])) {

					// Match found, store and break from loop
					$timezone = trim($matches[1]);
					break;

				}

			}

		}

		// Assign variables
		$this->smarty->assignByRef("TIMEZONE", $timezone);

		$rc = $this->updateIniFile(__DIR__ . "/php.tpl", Config::read("php72us|iniFile"), $this->smarty);
		if($rc === false)
			return false;

		$rc = $this->updateIniFile(dirname(__DIR__) . "/php/opcache.tpl", Config::read("php72us|opcacheFile"), $this->smarty);
		if($rc === false)
			return false;

		// Roundcube support
		$rcfg = Config::read("php72us|directoryFpmD") . "/roundcube.conf";
		if(Config::read("roundcube") === "enabled") {

			// Save roundcube php-fpm configuration
			$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/roundcube.tpl", $rcfg);
			if($rc === false)
				return false;

		} elseif(is_file($rcfg)) {

			// Delete roundcube php-fpm configuration
			unlink($rcfg);

		}

		// Get all virtual hosts and generate individual configurations
		$rc = $this->saveVirtualHosts();
		if($rc === false)
			return false;

		return true;

	}

	/**
	 * Enable module
	 * @return bool
	 */
	public function enable()
	{

		Log::debug("Enabling module: php72us");

		// Configuration
		Config::write("php72us", "enabled");
		Config::write("php", "disabled");

		// Switch to apache24us if apache is enabled
		if(Config::read("apache") === "enabled") {

			Config::write("apache", "disabled");
			Config::write("apache24us", "enabled");

		}

		// SystemD controller
		$this->systemCtl("enable");

		return true;

	}

	/**
	 * Disable module
	 * @return bool
	 */
	public function disable()
	{

		Log::debug("Disabling module: php72us");

		// Configuration
		Config::write("php72us", "disabled");
		Config::write("php", "enabled");

		// Switch to apache if apache24us is enabled
		if(Config::read("apache24us") === "enabled") {

			Config::write("apache24us", "disabled");
			Config::write("apache", "enabled");

		}

		// SystemD controller
		$this->systemCtl("disable");

		return true;

	}

}

