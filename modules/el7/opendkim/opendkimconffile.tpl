# Aetolos - Automatically generated OpenDKIM configuration

PidFile			/var/run/opendkim/opendkim.pid
Mode			sv
Syslog			yes
SyslogSuccess		yes
LogWhy			no
UserID			opendkim:opendkim
Socket			inet:8891@localhost
Umask			002
SendReports		no
SoftwareHeader		no
Canonicalization	relaxed/relaxed
Domain			*
Selector		default
MinimumKeyBits		1024
KeyFile			/etc/opendkim/keys/default.private
InternalHosts		refile:/etc/opendkim/TrustedHosts
OversignHeaders		From
#On-BadSignature	reject
#On-KeyNotFound		reject
On-Security		reject

