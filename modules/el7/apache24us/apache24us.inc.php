<?php
/**
 * Aetolos - Apache 2.4 IUS HTTPd
 *
 * Apache 2.4 IUS support and implementation
 *
 * @copyright Noumenia (C) 2018 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage apache24us
 */

/**
 * Apache24us class
 *
 * @package aetolos
 * @subpackage apache24us
 */
class Apache24us extends Apache {

	/**
	 * Smarty template engine
	 * @var Smarty|null
	 */
	private $smarty = null;

	/**
	 * Virtualhost template file
	 * @var string
	 */
	protected $vHostTemplate = "";

	/**
	 * Constructor.
	 * @return void
	 */
	public function __construct()
	{

		// Override virtual host template
		$this->vHostTemplate = __DIR__ . "/virtualhostapache.tpl";

	}

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"dependencies",
			"selinux",
			"saveVirtualHosts",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'apache24us'			=> "disabled",
			'apache|directoryConf'		=> "/etc/httpd/conf",
			'apache|directoryConfD'		=> "/etc/httpd/conf.d",
			'apache|directoryConfModulesD'	=> "/etc/httpd/conf.modules.d",
			'apache|maxClients'		=> 50,
			'apache|selinuxModule'		=> "",
			'apache|listenIp'		=> ""
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"httpd24u",
			"httpd24u-tools",
			"httpd24u-filesystem",
			"httpd24u-mod_ssl",
			"httpd24u-mod_security2",
			"httpd-itk",
			"mod_evasive"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array("php72us"),
			'repository'	=> array("ius-release"),
			'service'	=> array("httpd"),
			'conflict'	=> array(
				"httpd",
				"httpd-tools"
			)
		);

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: apache24us");

		// Smarty template
		if($this->smarty === null)
			$this->smarty = TemplateFactory::create();

		// Disable autoindex
		$file = Config::read("apache|directoryConfD") . "/autoindex.conf";
		if(is_file($file) && filesize($file) > 0) {
			rename($file, $file . ".disabled");
			touch($file);
		}

		// Disable welcome
		$file = Config::read("apache|directoryConfD") . "/welcome.conf";
		if(is_file($file) && filesize($file) > 0) {
			rename($file, $file . ".disabled");
			touch($file);
		}

		// Assign variables
		$this->smarty->assign("SERVERNAME", php_uname("n"));
		$this->smarty->assign("CERTS", Config::read("pkitls|directoryCerts"));
		$this->smarty->assign("KEYS", Config::read("pkitls|directoryPrivate"));
		if(Config::read("apache|listenIp") !== "")
			$this->smarty->assign("SERVERIP", Config::read("apache|listenIp"));
		else
			$this->smarty->assign("SERVERIP", gethostbyname(php_uname("n")));
		if(is_file(Config::read("pkitls|directoryCerts") . "/localhost.chain"))
			$this->smarty->assign("CHAIN", true);
		else
			$this->smarty->assign("CHAIN", false);
		if(is_file(Config::read("pkitls|directoryCerts") . "/localhost.cabundle"))
			$this->smarty->assign("CABUNDLE", true);
		else
			$this->smarty->assign("CABUNDLE", false);

		// Generate main configuration (httpd.conf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/httpdconf.tpl", Config::read("apache|directoryConf") . "/httpd.conf");
		if($rc === false)
			return false;

		// Generate ssl configuration (ssl.conf)
		$rc = $this->saveConfigFile($this->smarty, dirname(__DIR__) . "/apache/sslconf.tpl", Config::read("apache|directoryConfD") . "/ssl.conf");
		if($rc === false)
			return false;

		// Generate evasive configuration (mod_evasive.conf)
		$rc = $this->saveConfigFile($this->smarty, dirname(__DIR__) . "/apache/evasiveconf.tpl", Config::read("apache|directoryConfD") . "/mod_evasive.conf");
		if($rc === false)
			return false;

		// Generate base modules (00-base.conf)
		$rc = $this->saveConfigFile($this->smarty, dirname(__DIR__) . "/apache/baseconf.tpl", Config::read("apache|directoryConfModulesD") . "/00-base.conf");
		if($rc === false)
			return false;

		// Generate base modules (00-mpm.conf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/mpmconf.tpl", Config::read("apache|directoryConfModulesD") . "/00-mpm.conf");
		if($rc === false)
			return false;

		// Generate base modules (00-proxy.conf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/proxyconf.tpl", Config::read("apache|directoryConfModulesD") . "/00-proxy.conf");
		if($rc === false)
			return false;

		// Get all virtual hosts and generate individual configurations
		$rc = $this->saveVirtualHosts();
		if($rc === false)
			return false;

		// Generate mpm-itk configuration (00-mpm-itk.conf)
		$rc = $this->saveConfigFile($this->smarty, dirname(__DIR__) . "/apache/mpmitkconf.tpl", Config::read("apache|directoryConfModulesD") . "/00-mpm-itk.conf");
		if($rc === false)
			return false;

		return true;

	}

	/**
	 * Enable module
	 * @return bool
	 */
	public function enable()
	{

		// Return if Php72us is not enabled
		if(Config::read("php72us") !== "enabled") {
			Log::warning("Module is not enabled: php72us");
			return false;
		}

		Log::debug("Enabling feature: apache24us");

		// Configuration
		Config::write("apache24us", "enabled");
		Config::write("apache", "disabled");

		// SystemD controller
		$this->systemCtl("enable");

		return true;

	}

	/**
	 * Disable module
	 * @return bool
	 */
	public function disable()
	{

		Log::debug("Disabling feature: apache24us");

		// Configuration
		Config::write("apache24us", "disabled");

		// SystemD controller
		$this->systemCtl("disable");

		return true;

	}

}

