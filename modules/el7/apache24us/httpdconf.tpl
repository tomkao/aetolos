# Aetolos - Automatically generated Apache configuration

ServerRoot "/etc/httpd"
Listen 80

Include conf.modules.d/00-base.conf
Include conf.modules.d/00-mpm.conf
Include conf.modules.d/00-mpm-itk.conf
Include conf.modules.d/00-proxy.conf
Include conf.modules.d/00-ssl.conf
Include conf.modules.d/00-systemd.conf

User apache
Group apache
ServerAdmin webmaster@localhost
ServerName {$SERVERNAME}
TraceEnable Off
ServerSignature Off
ServerTokens ProductOnly
FileETag None
KeepAlive On
MaxKeepAliveRequests 100
MaxConnectionsPerChild 100000
Timeout 8
KeepAliveTimeout 4
ProxyTimeout 121

# small server (sys RAM: 512MB to 1024MB)
# ---------------------------------------
StartServers 5
MinSpareServers 5
MaxSpareServers 10
MaxRequestWorkers 50

## medium server (sys RAM: 1024MB to 2048MB)
## -----------------------------------------
#StartServers 15
#MinSpareServers 10
#MaxSpareServers 20
#MaxRequestWorkers 100

## large-ish server (sys RAM: 2048MB to 6144MB)
## --------------------------------------------
#StartServers 30
#MinSpareServers 15
#MaxSpareServers 30
#MaxRequestWorkers 200	<-- if you raise this above 256
#ServerLimit 256	<-- then you also need to raise this!

<Directory />
	AllowOverride none
	Require all denied
</Directory>

<IfModule dir_module>
	DirectoryIndex index.html index.php
</IfModule>

<Files ".ht*">
	Require all denied
</Files>

<IfModule mod_deflate.c>
	AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css application/javascript application/xml application/xhtml+xml application/rss+xml font/woff font/woff2 image/svg+xml
</IfModule>

<Files ~ "\.(css\.gz|js\.gz|css|js|jpg|jpeg|gif|png|bmp|tif|mov|mp4|mpg|mpeg|avi|mkv|pdf|ico|swf|webm|webp|woff|woff2|eot|svg|ttf)$">
	ExpiresActive on
	ExpiresDefault "access plus 1 month"
	Header unset Set-Cookie env=unset-cookie
</Files>

ErrorLog "logs/error_log"
LogLevel warn

<IfModule log_config_module>{literal}
	LogFormat "%a %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
	CustomLog "logs/access_log" combined
{/literal}</IfModule>

<IfModule mime_module>
	TypesConfig /etc/mime.types
	AddType application/x-compress .Z
	AddType application/x-gzip .gz .tgz
	AddType font/woff .woff
	AddType font/woff2 .woff2
	AddType text/html .shtml
	AddType text/html .php
	AddOutputFilter INCLUDES .shtml
</IfModule>

<Files ".user.ini">
	Require all denied
</Files>

SetEnvIfNoCase ^Authorization$ "(.+)" HTTP_AUTHORIZATION=$1

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
	MIMEMagicFile conf/magic
</IfModule>

<ifModule mod_autoindex.c>
	IndexIgnore .htaccess */.??* *~ *# */HEADER* */README* */_vti*
</IfModule>

<IfModule mod_headers.c>
	Header always set Referrer-Policy "strict-origin-when-cross-origin"
	Header always set X-Xss-Protection "1; mode=block"
	Header always set X-Content-Type-Options "nosniff"
	Header always set X-Frame-Options "SAMEORIGIN"
	Header always set Permissions-Policy "interest-cohort=()"
</IfModule>

EnableMMAP on
EnableSendfile on

<VirtualHost *:80>

	ServerName {$SERVERNAME}

	DocumentRoot /var/www/html
	UseCanonicalName Off

	AssignUserID apache apache

	<Directory "/var/www/html/.well-known">
		Require all granted
	</Directory>

</VirtualHost>

<VirtualHost *:443>

	ServerName {$SERVERNAME}

	DocumentRoot /var/www/html
	UseCanonicalName Off

	AssignUserID apache apache

	SSLEngine on
	SSLCertificateFile {$CERTS}/localhost.crt
	SSLCertificateKeyFile {$KEYS}/localhost.key
	{if isset($CHAIN) && $CHAIN===true}SSLCertificateChainFile {$CERTS}/localhost.chain{/if} 
	{if isset($CABUNDLE) && $CABUNDLE===true}SSLCACertificateFile {$CERTS}/localhost.cabundle{/if} 

	<IfModule mod_headers.c>
		Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains"
		Header always set Content-Security-Policy "default-src https:; object-src 'none'; frame-ancestors 'none'"
	</IfModule>

</VirtualHost>

IncludeOptional conf.d/*.conf

