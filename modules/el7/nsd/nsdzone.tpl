; Aetolos - Automatically generated NSD zone

$ORIGIN {$DOMAINNAME}.				; default zone domain
$TTL 86400					; default time to live

; ---------------------------------------------------
; NAME	CLASS	TYPE	NAME-SERVER	EMAIL-ADDRESS
; ---------------------------------------------------
@	IN	SOA	ns1		postmaster.{$DOMAINNAME}. (
				{$SERIAL}	; serial number
				28800		; refresh
				14400		; retry
				864000		; expire
				86400		; min TTL
				)

; ------------------------------------------------------------------
; NAME				CLASS		TYPE		DATA
; ------------------------------------------------------------------

{if isset($NAMESERVERS) && sizeof($NAMESERVERS)>0}
; name servers
; ------------

{foreach $NAMESERVERS as $N}
{$DOMAINNAME}.			IN		NS		{$N}.
{/foreach}
{/if}

{if isset($MAILEXCHANGE) && sizeof($MAILEXCHANGE)>0}
; mail exchange
; -------------

{foreach $MAILEXCHANGE as $M}
{$DOMAINNAME}.			IN		MX		{$WEIGHT} {$M}.
{/foreach}
{/if}

; additional records
; ------------------

{$DOMAINNAME}.			IN		A		{$IPADDRESS}
localhost			IN		A		127.0.0.1
mail				IN		A		{$IPADDRESS}
www				IN		A		{$IPADDRESS}

; catch-all record
; ----------------

;*				IN		A		{$IPADDRESS}

; txt record
; ----------

{$DOMAINNAME}.			IN		TXT		"v=spf1 +a +mx -all"
{if isset($OPENDKIM) && $OPENDKIM!=""}default._domainkey.{$DOMAINNAME}.			IN		TXT		"{$OPENDKIM}"
_dmarc.{$DOMAINNAME}.		IN		TXT		"v=DMARC1; p=reject; adkim=s; aspf=s; sp=reject"{/if}
_adsp._domainkey.{$DOMAINNAME}.	IN		TXT		"dkim=all"

