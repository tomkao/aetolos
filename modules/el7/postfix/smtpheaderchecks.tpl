# Aetolos - Automatically generated Postfix configuration

# Privacy - ignore received header from authenticated users
/^Received:.*ESMTPSA/   IGNORE

# Privacy - ignore client IP address
/^X-Originating-IP:/    IGNORE

