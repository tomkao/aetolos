# Aetolos - Automatically generated HAproxy configuration

net.ipv4.ip_local_port_range = 1025	60999
net.ipv4.tcp_max_syn_backlog = 100000
net.core.netdev_max_backlog = 100000
net.core.somaxconn = 65534
net.ipv4.tcp_tw_reuse = 1
net.netfilter.nf_conntrack_max = 131072

