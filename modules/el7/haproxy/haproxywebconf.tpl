# Aetolos - Automatically generated HAproxy configuration

global
    log                       127.0.0.1 local5
    chroot                    /var/lib/haproxy
    pidfile                   /var/run/haproxy.pid
    maxconn                   {$MAXCONN}
    user                      haproxy
    group                     haproxy
    daemon
    stats socket              /var/lib/haproxy/stats

    # SSL/TLS
    ca-base                   /etc/pki/tls/certs/
    crt-base                  /etc/haproxy/cert/
    ssl-default-bind-ciphers  ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK
    ssl-default-bind-options  no-sslv3 no-tlsv10 no-tlsv11 no-tls-tickets
    tune.ssl.cachesize        1024000
    tune.ssl.lifetime         1200
    tune.ssl.default-dh-param 2048

defaults
    log                       global
    mode                      http
    option                    redispatch
    option                    http-server-close
    option                    forwardfor
    option                    splice-auto
    option                    httplog
    option                    dontlognull
    retries                   2
    timeout connect           5s
    timeout queue             10s
    timeout client            30s
    timeout server            30s
    timeout http-request      3s
    balance                   leastconn
    cookie                    PROXY insert nocache httponly secure maxlife 3h
    compression algo          gzip deflate
    compression type          text/html text/plain text/xml text/css application/javascript application/xml application/xhtml+xml application/rss+xml font/woff font/woff2 image/svg+xml

frontend webFrontend_80
    bind                      *:80
    maxconn                   50
    redirect scheme           https code 301 if !{ ssl_fc }
    capture request header    Host len 36
    default_backend           webServers

frontend webFrontend_443
    bind                      *:443 ssl strict-sni crt /etc/haproxy/cert/
    http-response             set-header Strict-Transport-Security max-age=31536000
{if isset($ACME) && $ACME!==''}
    acl                       redirect_to_acme path_beg /.well-known/acme-challenge/
    use_backend               acmeServers if redirect_to_acme
{/if}
    capture request header    Host len 36
    default_backend           webServers

backend webServers
{if isset($SERVERS)}{foreach $SERVERS as $S}
    server web{$S@iteration} {$S} cookie web{$S@iteration}
{/foreach}{/if}

{if isset($ACME) && $ACME!==''}
backend acmeServers
    server acme1 {$ACME}
{/if}

{if isset($STATS) && $STATS!==''}
listen stats {$STATS}
    mode http
    stats enable
    stats uri /haproxy-web-stats
    stats realm HAProxy\ Statistics
    stats auth haproxy-web-stats:{$STATSPASSWORD}
    stats admin if TRUE
{/if}

