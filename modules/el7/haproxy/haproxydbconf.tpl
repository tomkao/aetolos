# Aetolos - Automatically generated HAproxy configuration

global
    log         127.0.0.1 local5
    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     {$MAXCONN}
    user        haproxy
    group       haproxy
    daemon
    stats socket /var/lib/haproxy/stats

defaults
    mode                    tcp
    log                     global
    option                  tcplog
    option                  dontlognull
    option                  redispatch
    retries                 3
    timeout connect         5s
    timeout client          1m
    timeout server          1m
    maxconn                 {$MAXCONN}
    balance                 leastconn

listen dbCluster *:3306
{if isset($SERVERS)}{foreach $SERVERS as $S}
    server db{$S@iteration} {$S}
{foreach}{/if}

{if isset($STATS) && $STATS!==''}
listen stats {$STATS}
    mode http
    stats enable
    stats uri /haproxy-db-stats
    stats realm HAProxy\ Statistics
    stats auth haproxy-db-stats:{$STATSPASSWORD}
    stats admin if TRUE
{/if}

