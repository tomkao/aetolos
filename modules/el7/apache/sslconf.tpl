# Aetolos - Automatically generated Apache configuration

Listen					443 https
SSLPassPhraseDialog			exec:/usr/libexec/httpd-ssl-pass-dialog
SSLSessionCache				shmcb:/run/httpd/sslcache(1024000)
SSLSessionCacheTimeout			300
SSLRandomSeed				startup file:/dev/urandom  256
SSLRandomSeed				connect builtin
SSLCryptoDevice				builtin
SSLProtocol				all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
SSLCipherSuite				ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
SSLHonorCipherOrder			on
SSLCompression				off
SSLStrictSNIVHostCheck			on

# Not yet supported (httpd >= 2.4.11)
#SSLSessionTickets			off

# Stapling requires the web server to perform outgoing connections to retrieve data! Disabled by default.
SSLUseStapling				off
SSLStaplingResponderTimeout		5
SSLStaplingReturnResponderErrors	off
SSLStaplingCache			shmcb:/run/httpd/ocsp(128000)

