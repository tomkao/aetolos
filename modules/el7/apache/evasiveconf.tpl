# Aetolos - Automatically generated Apache configuration

LoadModule evasive20_module modules/mod_evasive24.so
<IfModule mod_evasive24.c>
DOSHashTableSize	3097
DOSPageCount		20
DOSSiteCount		100
DOSPageInterval		1
DOSSiteInterval		1
DOSBlockingPeriod	300
#DOSEmailNotify		you@yourdomain.com
#DOSSystemCommand	"su - someuser -c '/sbin/... %s ...'"
#DOSLogDir		"/var/lock/mod_evasive"
DOSWhitelist		127.0.0.1
#DOSWhitelist		192.168.0.*
</IfModule>

