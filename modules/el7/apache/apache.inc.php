<?php
/**
 * Aetolos - Apache HTTPd
 *
 * Apache support and implementation
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage apache
 */

/**
 * Apache class
 *
 * @package aetolos
 * @subpackage apache
 */
class Apache implements ModuleInterface {

	/**
	 * Helper functions for working with configuration files (@see ConfigHelper trait)
	 */
	use ConfigHelper;

	/**
	 * Helper functions for working with systemd services (@see SystemdHelper trait)
	 */
	use SystemdHelper;

	/**
	 * Smarty template engine
	 * @var Smarty|null
	 */
	private $smarty = null;

	/**
	 * Virtualhost template file
	 * @var string
	 */
	protected $vHostTemplate = "";

	/**
	 * Constructor.
	 * @return void
	 */
	public function __construct()
	{

		// Override virtual host template
		$this->vHostTemplate = __DIR__ . "/virtualhostapache.tpl";

	}

	/**
	 * Supported module features
	 * @return array<string>
	 */
	public function features()
	{

		return array(
			"parameters",
			"packages",
			"selinux",
			"saveVirtualHosts",
			"saveConfiguration"
		);

	}

	/**
	 * Supported module parameters
	 * @return array<string, int|string>
	 */
	public function parameters()
	{

		return array(
			'apache'			=> "disabled",
			'apache|directoryConf'		=> "/etc/httpd/conf",
			'apache|directoryConfD'		=> "/etc/httpd/conf.d",
			'apache|directoryConfModulesD'	=> "/etc/httpd/conf.modules.d",
			'apache|maxClients'		=> 50,
			'apache|selinuxModule'		=> "",
			'apache|listenIp'		=> ""
		);

	}

	/**
	 * Required RPM packages
	 * @return array<string>
	 */
	public function packages()
	{

		return array(
			"httpd",
			"httpd-tools",
			"httpd-itk",
			"mod_ssl",
			"mod_evasive",
			"mod_security"
		);

	}

	/**
	 * Module dependency
	 * @return array{module: array<string>, repository: array<string>, service: array<string>, conflict: array<string>}
	 */
	public function dependencies()
	{

		return array(
			'module'	=> array(),
			'repository'	=> array(),
			'service'	=> array("httpd"),
			'conflict'	=> array(
				"httpd24u",
				"httpd24u-mod_ssl",
				"httpd24u-tools",
				"httpd24u-mod_security2"
			)
		);

	}

	/**
	 * Module command-line parameters
	 * @return array<string>
	 */
	public function cmdParameters()
	{

		return array();

	}

	/**
	 * Help
	 * @return array<string|array<string, string>>
	 */
	public function help()
	{

		return array();

	}

	/**
	 * Module controller
	 * @param array $cmdParameters Command-line parameters
	 * @return string|array<mixed>|bool
	 */
	public function controller($cmdParameters)
	{

		return true;

	}

	/**
	 * Setup SELinux requirements
	 * @param string $contexts Context output from semanage
	 * @return bool
	 */
	public function selinux($contexts)
	{

		// SELinux httpd access to ~/public_html
		if(preg_match("/public_html.*httpd_sys_rw_content_t/", $contexts) === 0)
			exec("/usr/sbin/semanage fcontext -a -t httpd_sys_rw_content_t '/home/.*/public_html(/.*)?'");

		// Remove old context
		if(strpos($contexts, "/home/.*/tmp(/.*)?                                 all files          system_u:object_r:httpd_sys_rw_content_t:s0") !== false) {
			exec("/usr/sbin/semanage fcontext -d -t httpd_sys_rw_content_t '/home/.*/tmp(/.*)?' 2>/dev/null");
			$contexts = str_replace("/home/.*/tmp(/.*)?                                 all files          system_u:object_r:httpd_sys_rw_content_t:s0", "", $contexts);
		}

		// SELinux httpd access to ~/tmp
		if(preg_match("/tmp.*httpd_sys_rw_content_t/", $contexts) === 0)
			exec("/usr/sbin/semanage fcontext -a -t httpd_sys_rw_content_t '/home/[^/]+/tmp(/.*)?'");

		// SELinux - allow network access via scripts executed by Apache, if this is disabled then scripts like Roundcube can't run
		$rc = exec("/usr/sbin/getsebool httpd_can_network_connect");
		if(
			$rc !== false &&
			strpos($rc, "--> on") === false
		) {

			Log::debug("SELinux: enable httpd_can_network_connect");

			// Enable network access
			exec("/usr/sbin/setsebool -P httpd_can_network_connect on");

		}

		// SELinux - allow Apache scripts to send emails
		$rc = exec("/usr/sbin/getsebool httpd_can_sendmail");
		if(
			$rc !== false &&
			strpos($rc, "--> on") === false
		) {

			Log::debug("SELinux: enable httpd_can_sendmail");

			// Enable emails
			exec("/usr/sbin/setsebool -P httpd_can_sendmail on");

		}

		// SELinux - allow Apache to access home files
		$rc = exec("/usr/sbin/getsebool httpd_read_user_content");
		if(
			$rc !== false &&
			strpos($rc, "--> on") === false
		) {

			Log::debug("SELinux: enable httpd_read_user_content");

			// Enable home access
			exec("/usr/sbin/setsebool -P httpd_read_user_content on");

		}

		// SELinux - allow Apache to access home directories
		$rc = exec("/usr/sbin/getsebool httpd_enable_homedirs");
		if(
			$rc !== false &&
			strpos($rc, "--> on") === false
		) {

			Log::debug("SELinux: enable httpd_enable_homedirs");

			// Enable home access
			exec("/usr/sbin/setsebool -P httpd_enable_homedirs on");

		}

		// SELinux - allow Apache full access to tmp files
		if(Config::read("apache|selinuxModule") !== "installed13") {

			Log::debug("SELinux: enable httpd_home_tmp_aetolos.pp");

			// Install SELinux module
			exec("/usr/sbin/semodule -i " . __DIR__ . "/httpd_home_tmp_aetolos.pp 2>/dev/null");

			// Remember the module has already been installed
			Config::write("apache|selinuxModule", "installed13");

		}

		return true;

	}

	/**
	 * Save virtual hosts
	 * @return bool
	 */
	public function saveVirtualHosts()
	{

		// Smarty template
		if($this->smarty === null)
			$this->smarty = TemplateFactory::create();

		$this->smarty->assign("CERTS", Config::read("pkitls|directoryCerts"));
		$this->smarty->assign("KEYS", Config::read("pkitls|directoryPrivate"));

		// Get all virtual hosts and generate individual configurations
		$vhosts = VirtualHostFactory::populate();

		// Loop 2 (generate virtual hosts)
		foreach($vhosts as &$v) {

			// Skip parked domains
			if($v->parkedUnder != "")
				continue;

			// Assign variables
			$this->smarty->assignByRef("SERVERNAME", $v->domainName);
			$this->smarty->assignByRef("UNIXNAME", $v->unixName);
			$this->smarty->assignByRef("IPADDRESS", $v->ipAddress);
			$this->smarty->assignByRef("HOME", $v->home);
			$this->smarty->assignByRef("PREFIXALIAS", $v->prefixAlias);
			if(is_file(Config::read("pkitls|directoryCerts") . "/" . $v->domainName . ".chain"))
				$this->smarty->assign("CHAIN", true);
			else
				$this->smarty->assign("CHAIN", false);
			if(is_file(Config::read("pkitls|directoryCerts") . "/" . $v->domainName . ".cabundle"))
				$this->smarty->assign("CABUNDLE", true);
			else
				$this->smarty->assign("CABUNDLE", false);
			if(isset($v->parkedDomains))
				$this->smarty->assignByRef("PARKEDDOMAINS", $v->parkedDomains);
			else
				$this->smarty->assign("PARKEDDOMAINS", array());

			// Save configuration file
			$rc = $this->saveConfigFile($this->smarty, $this->vHostTemplate, Config::read("apache|directoryConfD") . "/" . $v->domainName . ".conf");
			if($rc === false)
				return false;

		}

		return true;

	}

	/**
	 * Save configuration
	 * @return bool
	 */
	public function saveConfiguration()
	{

		Log::debug("Save configuration: apache");

		// Smarty template
		if($this->smarty === null)
			$this->smarty = TemplateFactory::create();

		// Disable autoindex
		$file = Config::read("apache|directoryConfD") . "/autoindex.conf";
		if(is_file($file) && filesize($file) > 0) {
			rename($file, $file . ".disabled");
			touch($file);
		}

		// Disable welcome
		$file = Config::read("apache|directoryConfD") . "/welcome.conf";
		if(is_file($file) && filesize($file) > 0) {
			rename($file, $file . ".disabled");
			touch($file);
		}

		// Assign variables
		$this->smarty->assign("SERVERNAME", php_uname("n"));
		$this->smarty->assign("CERTS", Config::read("pkitls|directoryCerts"));
		$this->smarty->assign("KEYS", Config::read("pkitls|directoryPrivate"));
		if(Config::read("apache|listenIp") !== "")
			$this->smarty->assign("SERVERIP", Config::read("apache|listenIp"));
		else
			$this->smarty->assign("SERVERIP", gethostbyname(php_uname("n")));
		if(is_file(Config::read("pkitls|directoryCerts") . "/localhost.chain"))
			$this->smarty->assign("CHAIN", true);
		else
			$this->smarty->assign("CHAIN", false);
		if(is_file(Config::read("pkitls|directoryCerts") . "/localhost.cabundle"))
			$this->smarty->assign("CABUNDLE", true);
		else
			$this->smarty->assign("CABUNDLE", false);

		// Generate main configuration (httpd.conf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/httpdconf.tpl", Config::read("apache|directoryConf") . "/httpd.conf");
		if($rc === false)
			return false;

		// Generate ssl configuration (ssl.conf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/sslconf.tpl", Config::read("apache|directoryConfD") . "/ssl.conf");
		if($rc === false)
			return false;

		// Generate evasive configuration (mod_evasive.conf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/evasiveconf.tpl", Config::read("apache|directoryConfD") . "/mod_evasive.conf");
		if($rc === false)
			return false;

		// Generate base modules (00-base.conf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/baseconf.tpl", Config::read("apache|directoryConfModulesD") . "/00-base.conf");
		if($rc === false)
			return false;

		// Get all virtual hosts and generate individual configurations
		$rc = $this->saveVirtualHosts();
		if($rc === false)
			return false;

		// Generate mpm-itk configuration (00-mpm-itk.conf)
		$rc = $this->saveConfigFile($this->smarty, __DIR__ . "/mpmitkconf.tpl", Config::read("apache|directoryConfModulesD") . "/00-mpm-itk.conf");
		if($rc === false)
			return false;

		return true;

	}

}

