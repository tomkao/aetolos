# Aetolos - Automatically generated Apache virtual host configuration

<VirtualHost *:80>

	ServerName {$SERVERNAME}
{if (isset($PREFIXALIAS) && $PREFIXALIAS!="") || (isset($PARKEDDOMAINS) && sizeof($PARKEDDOMAINS)>0)}
	ServerAlias{if isset($PREFIXALIAS) && $PREFIXALIAS!=""} {$PREFIXALIAS}.{$SERVERNAME}{/if}{if isset($PARKEDDOMAINS) && sizeof($PARKEDDOMAINS)>0}{foreach $PARKEDDOMAINS as $P} {$P->domainName}{if isset($P->prefixAlias) && $P->prefixAlias!=""} {$P->prefixAlias}.{$P->domainName}{/if}{/foreach}{/if}{/if}
	DocumentRoot {$HOME}/{$UNIXNAME}/public_html
	ServerAdmin webmaster@{$SERVERNAME}
	UseCanonicalName Off
	Alias /.well-known /var/www/html/.well-known
	<Directory "/var/www/html/.well-known">
		Require all granted
	</Directory>

	AssignUserID {$UNIXNAME} {$UNIXNAME}
	{if isset($MAXCLIENTS) && $MAXCLIENTS!=''}MaxClientsVHost {$MAXCLIENTS}{/if}
	RewriteEngine On
	RewriteCond {literal}%{REQUEST_URI}{/literal} !^/\.well-known/acme-challenge/
	RewriteRule .* https://{literal}%{HTTP_HOST}%{REQUEST_URI}{/literal} [R=permanent,L]

</VirtualHost>

<VirtualHost *:443>

	ServerName {$SERVERNAME}
{if (isset($PREFIXALIAS) && $PREFIXALIAS!="") || (isset($PARKEDDOMAINS) && sizeof($PARKEDDOMAINS)>0)}
	ServerAlias{if isset($PREFIXALIAS) && $PREFIXALIAS!=""} {$PREFIXALIAS}.{$SERVERNAME}{/if}{if isset($PARKEDDOMAINS) && sizeof($PARKEDDOMAINS)>0}{foreach $PARKEDDOMAINS as $P} {$P->domainName}{if isset($P->prefixAlias) && $P->prefixAlias!=""} {$P->prefixAlias}.{$P->domainName}{/if}{/foreach}{/if}{/if}
	DocumentRoot {$HOME}/{$UNIXNAME}/public_html
	ServerAdmin webmaster@{$SERVERNAME}
	UseCanonicalName Off
	Options -ExecCGI -Includes
	RemoveHandler cgi-script .cgi .pl .plx .ppl .perl
	CustomLog /var/log/httpd/{$SERVERNAME}_log combined

	php_admin_value open_basedir "{$HOME}/{$UNIXNAME}/:/usr/lib/php/:/usr/lib64/php/:/usr/share/php/:/usr/local/lib/php/:/usr/local/lib64/php/:/tmp/:/usr/share/pear/:/usr/share/pear-data/:/usr/share/GeoIP/"
	php_admin_value upload_tmp_dir "{$HOME}/{$UNIXNAME}/tmp"
	php_admin_value session.save_path "{$HOME}/{$UNIXNAME}/tmp"
	php_admin_value sys_temp_dir "{$HOME}/{$UNIXNAME}/tmp"
	php_admin_value soap.wsdl_cache_dir "{$HOME}/{$UNIXNAME}/tmp"

	AssignUserID {$UNIXNAME} {$UNIXNAME}
	{if isset($MAXCLIENTS) && $MAXCLIENTS!=''}MaxClientsVHost {$MAXCLIENTS}{/if}
	SSLEngine on
	SSLCertificateFile {$CERTS}/{$SERVERNAME}.crt
	SSLCertificateKeyFile {$KEYS}/{$SERVERNAME}.key
	{if isset($CHAIN) && $CHAIN===true}SSLCertificateChainFile {$CERTS}/{$SERVERNAME}.chain
	{/if}
	{if isset($CABUNDLE) && $CABUNDLE===true}SSLCACertificateFile {$CERTS}/{$SERVERNAME}.cabundle
	{/if}

	<IfModule mod_headers.c>
		Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains"
		#Header always set Content-Security-Policy "default-src *; object-src 'self'; frame-ancestors 'none'"
		Header always set Permissions-Policy "interest-cohort=()"
	</IfModule>

	<Directory "{$HOME}/{$UNIXNAME}/public_html">
		AllowOverride All
	</Directory>

	#Include "/etc/httpd/conf.d/{$SERVERNAME}/*.conf"

</VirtualHost>

