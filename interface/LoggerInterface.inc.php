<?php
/**
 * Aetolos - Log interface
 *
 * An interface which defines an abstraction usage for logging events.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage loggerinterface
 */

/**
 * Logger interface
 *
 * @package aetolos
 * @subpackage loggerinterface
 */
interface LoggerInterface {

	/**
	 * Set destination.
	 * @param LogDestinationInterface $destination Log destination object
	 * @param int $minLevel Minimum priority level to log, lower levels are ignored
	 * @param array<string> $defaultParameters Default parameters to pass to the destination writer, in the form of an array of strings
	 * @return void
	 */
	public static function setDestination($destination, $minLevel = null, $defaultParameters = array());

	/**
	 * Emergency
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function emergency($message, $parameters = array());

	/**
	 * Alert
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function alert($message, $parameters = array());

	/**
	 * Critical
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function critical($message, $parameters = array());

	/**
	 * Error
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function error($message, $parameters = array());

	/**
	 * Warning
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function warning($message, $parameters = array());

	/**
	 * Notice
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function notice($message, $parameters = array());

	/**
	 * Info
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function info($message, $parameters = array());

	/**
	 * Debug
	 * @param string $message Message to log
	 * @param array<string> $parameters Extra parameters
	 * @return void
	 */
	public static function debug($message, $parameters = array());

}

