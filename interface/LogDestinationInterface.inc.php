<?php
/**
 * Aetolos - Log destination interface
 *
 * Log events
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage logdestination
 */

/**
 * Log destination interface
 *
 * @package aetolos
 * @subpackage logdestination
 */
interface LogDestinationInterface {

	/**
	 * Constructor.
	 * @param string $output Normal or error output
	 * @return void
	 */
	public function __construct($output = "");

	/**
	 * Write string to output
	 * @param string $string Message string
	 * @param int $priorityLevel Priority level
	 * @param array<string> $parameters Extra parameters to pass to the destination writer, in the form of an array of strings
	 * @return void
	 */
	public function write($string, $priorityLevel, $parameters = array());

}

