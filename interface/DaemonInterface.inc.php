<?php
/**
 * Aetolos - Daemon interface
 *
 * An interface which defines a cli daemon.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage daemon
 */

/**
 * Daemon manager interface
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage daemonmanager
 */
interface DaemonInterface
{

	/**
	 * Constructor
	 * @param string $filePid PID file
	 * @return void
	 */
	public function __construct(string $filePid = "");

	/**
	 * Check the process limit
	 * @return bool
	 */
	public function checkProcessLimit(): bool;

	/**
	 * Fork process and store the new PID within the process list array
	 * @return int
	 */
	public function fork(): int;

	/**
	 * Signal handler
	 * @param int $signal Signal code
	 * @param mixed $siginfo Signal information
	 * @return void
	 */
	public function signalHandler(int $signal, $siginfo);

}

