<?php
/**
 * Aetolos - Socket manager interface
 *
 * An interface which defines a cli socket manager.
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage socketmanager
 */

/**
 * Socket manager interface
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage socketmanager
 */
interface SocketInterface
{

	/**
	 * Accept connections
	 * @return bool
	 */
	public function acceptConnections(): bool;

	/**
	 * Read HTTP formatted string from stream socket
	 * @return array{method: string, request-uri: string, http-version: string, header: array<string>, body: string, status-code: int}
	 */
	public function socketReadByHttp(): array;

	/**
	 * Write HTTP formatted string to stream socket
	 * @param array{http-version: string, status-code: int, reason-phrase: string, header: array<string>, body: string} $message Message
	 * @return bool
	 */
	public function socketWriteByHttp(array $message): bool;

	/**
	 * Read string terminated by a NUL from socket
	 * @return string
	 */
	public function socketReadByNul(): string;

	/**
	 * Write string terminated by NUL to socket
	 * @param string $message Message
	 * @return bool
	 */
	public function socketWriteByNul(string $message): bool;

	/**
	 * Read string prefixed by the size from socket (uint32)
	 * @return string
	 */
	public function socketReadBySize(): string;

	/**
	 * Write string prefixed by the size to socket (uint32)
	 * @param string $message Message
	 * @param bool $includesSizePrefix True implies that the message already includes a size prefix
	 * @return bool
	 */
	public function socketWriteBySize(string $message, bool $includesSizePrefix = false): bool;

}

