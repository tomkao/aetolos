<?php
/**
 * Aetolos - Enable
 *
 * Enable module
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage enable
 */

/** @var array<string, string> $cmdParameters */

// No direct access - loadable only
if(
	!defined("AET_IN") ||
	!isset($cmdParameters['enable'])
)
	die("No Access");

// Get dependencies
$deps = Config::$modules[$cmdParameters['enable']]->dependencies()['module'];

// Override package names for PHP 7.2 IUS
if(
	Config::read("distro") === "el7" &&
	Config::read("php72us") === "enabled"
)
	/** @var array<string> $deps */
	$deps = preg_replace(array('/^php$/i', '/^apache$/i'), array("php72us", "apache24us"), $deps);

// Loop dependencies
foreach($deps as $moduleName) {

	// Skip already enabled modules
	if(Config::read($moduleName) === "enabled")
		continue;

	Log::warning("Enabling dependency: " . $moduleName);

	// Enable module
	Config::$modules[$moduleName]->enable();

}

// Enable module
Config::$modules[$cmdParameters['enable']]->enable();

