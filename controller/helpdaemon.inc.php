<?php
/**
 * Aetolos - Help Daemon
 *
 * Display help for daemon parameters and arguments
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage help
 */

// No direct access - loadable only
if(!defined("AET_IN"))
	die("No Access");

// Display logo
echo chr(27) . "[34;1mAetolos" . chr(27) . "[0m v" . AET_VER . " " . AET_COPYRIGHT . ", " . AET_LICENSE . "\n";

// Premature exit for version only
if(
	isset($cmdParameters['V']) ||
	isset($cmdParameters['version'])
)
	exit();

echo "\nUsage: aetolosd [OPTION]\n\n";

// Array of help parameters
$help = array(
	array('short' => "V",	'long' => "version",			'desc' => "Display version information only"),
	array('short' => "h",	'long' => "help",			'desc' => "Display help about parameters"),
	array('short' => "v",	'long' => "verbose",			'desc' => "Enable verbose output to stdout"),
	array('short' => "c",	'long' => "config=[FILE]",		'desc' => "Set the configuration file (default is ./aetolosd.conf)"),
	""
);

// Display help
foreach($help as $h) {

	if(is_string($h))
		echo $h . "\n";
	else
		echo "  -" . $h['short'] . ",  --" . str_pad($h['long'], 40, " ", STR_PAD_RIGHT) . $h['desc'] . "\n";

}

