<?php
/**
 * Aetolos - Disable
 *
 * Disable module
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage disable
 */

/** @var array<string, string> $cmdParameters */

// No direct access - loadable only
if(
	!defined("AET_IN") ||
	!isset($cmdParameters['disable'])
)
	die("No Access");

// Find dependencies
foreach(Config::$modules as $moduleName => &$module) {

	// Skip requested and already disabled modules
	if(
		$moduleName === $cmdParameters['disable'] ||
		Config::read($moduleName) === "disabled"
	)
		continue;

	// Get dependencies
	$deps = $module->dependencies()['module'];

	// Skip empty
	if(sizeof($deps) < 1)
		continue;

	// Override package names for PHP 7.2 IUS
	if(
		Config::read("distro") === "el7" &&
		Config::read("php72us") === "enabled"
	)
		/** @var array<string> $deps */
		$deps = preg_replace(array('/^php$/i', '/^apache$/i'), array("php72us", "apache24us"), $deps);

	// Report matches
	if(in_array($cmdParameters['disable'], $deps)) {

		Log::warning("Disabling dependency: " . $moduleName);

		// Disable module
		$module->disable();

	}

}

// Disable module
Config::$modules[$cmdParameters['disable']]->disable();

