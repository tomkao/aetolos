<?php
/**
 * Aetolos - Export
 *
 * Export Aetolos backup file
 *
 * Backup analysis:
 *
 * DIRECTORY		| Description
 * -------------------------------------------------------
 * cp			= account parameters
 * cron			= cron jobs
 * dehydrated		= dehydrated module
 * homedir		= home directory
 * mysql		= mariadb module
 *
 * FILE			| Description
 * -------------------------------------------------------
 * homedir_paths	= Path to user home directory
 * mysql.sql		= SQL user priviledges
 * quota		= account quota in MB
 * ssldomain		= virtual host domain name
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage import
 */

/** @var array<string, string> $cmdParameters */

// No direct access - loadable only
if(!defined("AET_IN"))
	die("No Access");

// Check for parameter
if(!isset($cmdParameters['virtualhost'])) {

	Log::error("Missing parameter");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

// Load virtualhost
$vhost = new VirtualHostManager();
$vhost->domainName = $cmdParameters['virtualhost'];
$rc = $vhost->get();
if($rc === false) {

	Log::error("Unknown virtual host");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

// Create temporary directory and subdirectories
$tdir = sys_get_temp_dir() . "/" . rand() . "/" . $vhost->unixName . "/";
$rc = mkdir($tdir, 0700, true);
if($rc === false) {

	Log::error("Could not create temporary directory: " . $tdir);

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

mkdir($tdir . "cp", 0700);
mkdir($tdir . "cron", 0700);
mkdir($tdir . "mysql", 0700);
mkdir($tdir . "va", 0700);

Log::debug("Temporary directory set to: " . $tdir);

// Get related parked domains
$pDomains = $vhost->relatedParkedDomains();

// Backup file must not exist
if(is_file($cmdParameters['export'])) {

	Log::error("Backup file already exists");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

} elseif(strrpos($cmdParameters['export'], ".tar.gz") === false) {

	Log::error("Backup file must be a tar.gz");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

Log::debug("Export virtual host to: " . $cmdParameters['export']);

// Tar archive name
$tar = str_replace(".tar.gz", ".tar", $cmdParameters['export']);

// Tar home directory
Log::debug("Taring home directory: " . $vhost->home . "/" . $vhost->unixName);

/*
 * TAR command line explanation
 *
 * --transform=EXPRESSION	use sed replace EXPRESSION to transform file names
 * --force-local		archive file is local even if it has a colon
 * --recursion			recurse into directories
 * -c				create a new archive
 * --owner=NAME[:UID]		force NAME as owner for added files
 * --group=NAME[:GID]		force NAME as group for added files
 * --file=ARCHIVE		use archive file or device ARCHIVE
 * --directory=DIR		change to directory DIR
 */
exec("/usr/bin/tar" .
	" --transform=\"s,^," . $vhost->unixName . "/,S\"" .
	" --transform=\"s," . $vhost->unixName . "/.," . $vhost->unixName . "/homedir,S\"" .
	" --force-local" .
	" --recursion" .
	" -c" .
	" --owner=nobody" .
	" --group=nobody" .
	" --file=" . escapeshellarg($tar) .
	" --directory=" . escapeshellarg($vhost->home . "/" . $vhost->unixName . "/") .
	" .");

Log::debug("Generating setup files");

// Generate ssldomain file
file_put_contents($tdir . "ssldomain", $vhost->domainName);

// Generate quota file
file_put_contents($tdir . "quota", strval($vhost->quota));

// Generate homedir_paths file
file_put_contents($tdir . "homedir_paths", $vhost->home . "/" . $vhost->unixName);

// Copy cron file
$cron = "/var/spool/cron/" . $vhost->unixName;
if(is_file($cron))
	copy($cron, $tdir . "cron/" . $vhost->unixName);

// Loop parked domains
for($i = 0; $i < sizeof($pDomains); $i++)
	$pDomains[$i] = "DNS" . $i . "=" . $pDomains[$i];

if(sizeof($pDomains) > 0)
	$pDomains = implode("\n", $pDomains);
else
	$pDomains = "";

// Control parameters
file_put_contents($tdir . "cp/" . $vhost->unixName, "DNS=" . $vhost->domainName . "\nCONTACTEMAIL=" .
	$vhost->adminEmail . "\nUSER=" . $vhost->unixName . "\n" . $pDomains . "\n");

// Loop modules
foreach(Config::$modules as $moduleName => &$module) {

	// Skip disabled modules
	if(Config::read($moduleName) !== "enabled")
		continue;

	Log::debug("Module export: " . $moduleName);

	// Export virtual host
	$rc = $module->export($vhost, $tdir, $cmdParameters);
	if($rc === false) {

		// Remove temporary directory
		exec("/usr/bin/rm -rf " . escapeshellarg(dirname($tdir)));

		if(
			Config::read("daemon") === true &&
			isset($this)
		) {

			$this->sendHttpError(500);
			return true;

		} else {

			exit(9);

		}

	}

}

Log::debug("Taring setup files");

// Add to tar archive
exec("/usr/bin/tar --force-local --recursion --append -p --file=" . escapeshellarg($tar) . " --directory=" .
	escapeshellarg(dirname($tdir)) . " " . $vhost->unixName);

Log::debug("Gzip tar archive");

// Gzip tar archive
exec("/usr/bin/gzip -9 " . escapeshellarg($tar));

// Remove temporary directory
exec("/usr/bin/rm -rf " . escapeshellarg(dirname($tdir)));

