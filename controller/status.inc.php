<?php
/**
 * Aetolos - Status
 *
 * Display module status information
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage status
 */

// No direct access - loadable only
if(!defined("AET_IN"))
	die("No Access");

// Status array
$moduleStatus = array();

// Loop modules and populate status array
foreach(Config::$modules as $moduleName => &$module)
		$moduleStatus[$moduleName] = $module->status();

// JSON output
if(Config::read("json") === true) {

	// Create an array structure
	$statusArray = array(
		'system'	=> array(
			array(
				"System",
				Config::read("opname")
			),
			array(
				"Memory",
				floor(Config::read("memtotal") / 1024 / 1024) . " MiB"
			),
			array(
				"CPU cores",
				Config::read("cpucores")
			),
			array(
				"PHP version",
				phpversion()
			)
		),
		'status'	=> array(
			'Module'	=> array(),
			'Enabled'	=> array(),
			'SystemD'	=> array(),
			'Repository'	=> array(),
			'Dependencies'	=> array()
		)
	);

	// Apache module
	if(Config::read("apache") === "enabled") {

		exec("/usr/sbin/httpd -v", $httpd);
		if(isset($httpd[0])) {

			$rc = preg_match('/^Server version: Apache\/(.*?) /im', $httpd[0], $matches);
			if(
				$rc === 1 &&
				isset($matches[1])
			) {

				$statusArray['system'][] = array(
					"Apache",
					trim($matches[1])
				);

			}

		}

	}

	// MariaDB module
	if(Config::read("mariadb") === "enabled") {

		exec("/usr/bin/mysql --version", $mariadb);
		if(isset($mariadb[0])) {

			$rc = preg_match('/Distrib (.*?)-MariaDB/', $mariadb[0], $matches);
			if(
				$rc === 1 &&
				isset($matches[1])
			) {

				$statusArray['system'][] = array(
					"MariaDB",
					trim($matches[1])
				);

			}

		}

	}

	// Loop modules
	foreach(Config::$modules as $moduleName => &$module) {

		// Module name
		$statusArray['status']['Module'][] = $moduleName;

		// Enabled/disabled status
		if(Config::read($moduleName, true) === "enabled")
			$statusArray['status']['Enabled'][$moduleName] = true;
		else
			$statusArray['status']['Enabled'][$moduleName] = false;

		// SystemD status
		$statusArray['status']['SystemD'][$moduleName] = $moduleStatus[$moduleName];

		// Required repository
		$statusArray['status']['Repository'][$moduleName] = str_replace("-release", "", implode(", ", $module->dependencies()['repository']));

		// Dependencies
		$statusArray['status']['Dependencies'][$moduleName] = implode(", ", $module->dependencies()['module']);

	}

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		// no-op

	} else {

		// Generate a JSON encoded object
		echo json_encode($statusArray);

	}

} else {

	// Columns
	$columns = array(
		array(
			'arrayKey'	=> 0
		),
		array(
			'arrayKey'	=> 1
		)
	);

	// System information
	$data = array(
		array(
			"System",
			Config::read("opname")
		),
		array(
			"Memory",
			floor(Config::read("memtotal") / 1024 / 1024) . " MiB"
		),
		array(
			"CPU cores",
			Config::read("cpucores")
		),
		array(
			"PHP version",
			phpversion()
		)
	);

	// Apache module
	if(Config::read("apache") === "enabled") {

		exec("/usr/sbin/httpd -v", $httpd);
		if(isset($httpd[0])) {

			$rc = preg_match('/^Server version: Apache\/(.*?) /im', $httpd[0], $matches);
			if(
				$rc === 1 &&
				isset($matches[1])
			) {

				$data[] = array(
					"Apache",
					trim($matches[1])
				);

			}

		}

	}

	// MariaDB module
	if(Config::read("mariadb") === "enabled") {

		exec("/usr/bin/mysql --version", $mariadb);
		if(isset($mariadb[0])) {

			$rc = preg_match('/Distrib (.*?)-MariaDB/', $mariadb[0], $matches);
			if(
				$rc === 1 &&
				isset($matches[1])
			) {

				$data[] = array(
					"MariaDB",
					trim($matches[1])
				);

			}

		}

	}

	// Clean-up
	unset($httpd, $mariadb, $matches);

	Ascii::drawTable($columns, $data);

	// Columns
	$columns = array(
		array(
			'header'	=> "Enabled",
			'arrayKey'	=> 0,
			'maxSize'	=> 8
		),
		array(
			'header'	=> "Module",
			'arrayKey'	=> 1,
			'maxSize'	=> 7
		),
		array(
			'header'	=> "SystemD",
			'arrayKey'	=> 2,
			'maxSize'	=> 8
		),
		array(
			'header'	=> "Repository",
			'arrayKey'	=> 3,
			'maxSize'	=> 11
		),
		array(
			'header'	=> "Dependencies",
			'arrayKey'	=> 4,
			'maxSize'	=> 13
		)
	);

	// Start with an empty data array
	$data = array();

	// Loop modules
	foreach(Config::$modules as $moduleName => &$module) {

		$row = array();

		// Enabled/disabled status
		if(Config::read($moduleName, true) === "enabled") {

			$row[0] = true;


		} else {

			$row[0] = false;

			// Ignore dead status on disabled modules
			if($moduleStatus[$moduleName] === "dead")
				$moduleStatus[$moduleName] = "";

		}

		// Module name
		$row[1] = $moduleName;

		// SystemD status
		$row[2] = $moduleStatus[$moduleName];

		// Required repository
		$row[3] = str_replace("-release", "", implode(", ", $module->dependencies()['repository']));

		// Dependencies
		$row[4] = implode(", ", $module->dependencies()['module']);

		// Add to the data array
		$data[] = $row;

	}

	// Clean-up
	unset($moduleStatus, $moduleName, $module, $row);

	Ascii::drawTable($columns, $data);

}

