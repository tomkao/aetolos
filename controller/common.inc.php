<?php
/**
 * Aetolos - Common
 *
 * Common functionality
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage common
 */

// No direct access - loadable only
if(!defined("AET_IN"))
	die("No Access");

// Initialize constants
require_once(dirname(__DIR__) . "/library/Constants.inc.php");

// Initialize the autoloader
require_once(dirname(__DIR__) . "/library/NoumeniaAetolosAutoloader.inc.php");

// Register the autoloader
spl_autoload_register("NoumeniaAetolosAutoloader", true);

// Handle potential PHP errors
if(
	isset($cmdParameters['v']) ||
	isset($cmdParameters['verbose'])
) {

	// Enable error reporting to stderr
	ini_set("display_errors", "1");
	ini_set("display_startup_errors", "1");
	error_reporting(E_ALL);

} else {

	// Disable error reporting
	error_reporting(0);

}

// Set global configuration array with default values
Config::setDefaults();

// Initialize logging
require_once(dirname(__DIR__) . "/controller/loginit.inc.php");

// Detect running system
Config::detectSystem();

// Detect system memory
Config::detectMemory();

// Detect CPU cores
Config::detectCores();

// Detect proxy
Config::detectProxy();

// Initialize loadable modules
require_once(dirname(__DIR__) . "/controller/moduleinit.inc.php");

// Initialize database
require_once(dirname(__DIR__) . "/controller/databaseinit.inc.php");

