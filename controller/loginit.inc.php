<?php
/**
 * Aetolos - Log Initialization
 *
 * Initialize log reporting
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage loginit
 */

// No direct access - loadable only
if(!defined("AET_IN"))
	die("No Access");

// Set JSON format for log messages
if(
	isset($cmdParameters['j']) ||
	isset($cmdParameters['json'])
) {

	$defaultParameters = array("json");

	// Set JSON output
	Config::write("json", true);

} else {

	$defaultParameters = array();

}

// Set destination of log messages
if(
	isset($cmdParameters['q']) ||
	isset($cmdParameters['quiet'])
) {

	// Quiet - discard all output
	Log::setDestination(new LogDestinationNull("null"));

} elseif(
	isset($cmdParameters['v']) ||
	isset($cmdParameters['verbose'])
) {

	// Verbose output to console
	Log::setDestination(new LogDestinationConsole("stdout"), LOG_DEBUG, $defaultParameters);

	// Set verbose mode
	Config::write("verbose", true);

} else {

	// Display warnings or higher priority messages to console
	Log::setDestination(new LogDestinationConsole("stdout"), LOG_WARNING, $defaultParameters);

}

// Clean-up
unset($defaultParameters);

