<?php
/**
 * Aetolos - Setup
 *
 * Perform first time installation or setup existing system
 *
 * @copyright Noumenia (C) 2015 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package aetolos
 * @subpackage setup
 */

// No direct access - loadable only
if(!defined("AET_IN"))
	die("No Access");

// Class from distribution name
$distroClass = "Setup" . ucfirst(Config::read("distro"));

// Instantiate the setup
$setup = new $distroClass();
if(!method_exists($setup, "run")) {

	Log::error("Encountered an error while loading the setup class");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

// Perform operating system setup
$rc = $setup->run();
if($rc === false) {

	Log::error("Encountered an error while performing system setup");

	if(
		Config::read("daemon") === true &&
		isset($this)
	) {

		$this->sendHttpError(500);
		return true;

	} else {

		exit(9);

	}

}

